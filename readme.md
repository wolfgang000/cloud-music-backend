dotnet ef migrations add InitialCreate
dotnet ef database update

# Setup testing files

b2 sync --threads 5 b2://cloud-music-testing-assets/testing-assets/ ./CloudMusicTests/assets

# Build production image localy

docker build -t myimage -f Dockerfile.prod .

## Test production build

docker run --net=host --env-file ./.env.example.prod myimage

### Environment variables

The list of the required environment variables are available in example.env.prod

The docker compose build also requires this values,
copy example.env to a file local.env and set the values

In case you need to run the app directly load the enviroments variables

```
set -o allexport; source .env.develop; set +o allexport
set -o allexport; source .env.testing; set +o allexport
```

## Deploy

```
dokku apps:create cloud-music-back
dokku storage:mount cloud-music-back /var/lib/dokku/data/storage/cloud-music-back:/storage
dokku apps:create cloud-music-front
dokku config:set cloud-music-front NGINX_ROOT=dist
dokku plugin:install https://github.com/dokku/dokku-postgres.git
dokku postgres:create cloud-music-db
dokku postgres:link cloud-music-db cloud-music-back
dokku docker-options:add cloud-music-back build '--file Dockerfile.prod'
dokku config:set cloud-music-back \
    APP_FRONT=cloud-music-front-5000 \
    ASPNETCORE_TEMP_FILES_PATH="/storage/" \
    ASPNETCORE_OBJECT_STORAGE_PROVIDER=b2 \
    ASPNETCORE_B2_KEY_ID=123 \
    ASPNETCORE_B2_APPLICATION_KEY=secret \
    ASPNETCORE_B2_BUCKET_ID=123 \
    ASPNETCORE_B2_MEDIA_URL="https://example.com/file/cloud-music-testing" \
    ASPNETCORE_DB_HOST=localhost \
    ASPNETCORE_DB_PORT=5432 \
    ASPNETCORE_DB_USER=test_user \
    ASPNETCORE_DB_PASSWORD=test_password \
    ASPNETCORE_DB_NAME=test_db

dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
dokku config:set --no-restart cloud-music-back DOKKU_LETSENCRYPT_EMAIL=test@mail.com
dokku letsencrypt cloud-music-back

```

Ref Heroku DATABASE_URL: postgres://{user}:{password}@{hostname}:{port}/{database-name}
