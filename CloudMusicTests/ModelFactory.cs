using System;
using CloudMusic.Models;
using FizzWare.NBuilder;
namespace CloudMusicTest
{
    public static class ModelFactory
    {
        public static ISingleObjectBuilder<User> User()
        {
            Random rnd = new Random();
            return Builder<User>.CreateNew()
                .With(a => a.Id = rnd.Next())
                .With(u => u.Email = Faker.Internet.Email())
                .With(u => u.Name = Faker.Name.FullName())
                .With(u => u.StorageLimitSize = 100000000);
        }

        public static ISingleObjectBuilder<Artist> Artist()
        {
            Random rnd = new Random();
            return Builder<Artist>.CreateNew()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Name = Faker.Name.FullName());
        }

        public static IOperable<Artist> ArtistList(int size)
        {
            Random rnd = new Random();
            return Builder<Artist>.CreateListOfSize(size).All()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Name = Faker.Name.FullName());
        }

        public static ISingleObjectBuilder<Genre> Genre()
        {
            Random rnd = new Random();
            return Builder<Genre>.CreateNew()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Name = Faker.Name.FullName());
        }

        public static IOperable<Genre> GenreList(int size)
        {
            Random rnd = new Random();
            return Builder<Genre>.CreateListOfSize(size).All()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Name = Faker.Name.FullName());
        }

        public static ISingleObjectBuilder<Album> Album()
        {
            Random rnd = new Random();
            return Builder<Album>.CreateNew()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Title = Faker.Lorem.GetFirstWord())
                .With(a => a.Artist = ModelFactory.Artist().Build())
                .With(a => a.User = ModelFactory.User().Build());
        }

        public static IOperable<Album> AlbumList(int size)
        {
            Random rnd = new Random();
            return Builder<Album>.CreateListOfSize(size).All()
                .With(a => a.Id = rnd.Next())
                .With(a => a.Title = Faker.Lorem.GetFirstWord())
                .With(a => a.Artist = ModelFactory.Artist().Build())
                .With(a => a.User = ModelFactory.User().Build());
        }

        public static ISingleObjectBuilder<Track> Track()
        {
            Random rnd = new Random();
            return Builder<Track>.CreateNew()
                .With(t => t.Id = rnd.Next())
                .With(t => t.Title = Faker.Lorem.GetFirstWord())
                .With(t => t.SHA1Hash = rnd.Next().ToString())
                .With(t => t.Album = ModelFactory.Album().Build())
                .With(t => t.Artist = ModelFactory.Artist().Build())
                .With(t => t.Genre = ModelFactory.Genre().Build())
                .With(t => t.User = ModelFactory.User().Build());
        }
        public static IOperable<Track> TrackList(int size)
        {
            Random rnd = new Random();
            return Builder<Track>.CreateListOfSize(size).All()
                .With(t => t.Id = rnd.Next())
                .With(t => t.Title = Faker.Lorem.GetFirstWord())
                .With(t => t.SHA1Hash = rnd.Next().ToString())
                .With(t => t.Album = ModelFactory.Album().Build())
                .With(t => t.Artist = ModelFactory.Artist().Build())
                .With(t => t.Genre = ModelFactory.Genre().Build())
                .With(t => t.User = ModelFactory.User().Build());
        }
    }
}