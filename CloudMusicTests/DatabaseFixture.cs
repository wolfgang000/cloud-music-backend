using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Respawn;
using Xunit;
using CloudMusic.Utils;

namespace CloudMusicTest
{
    public class DatabaseFixture : IDisposable
    {
        private readonly string _connectionString;
        public readonly DbContextOptions<CloudMusicContext> dbContextOptions;
        private readonly Checkpoint _checkpoint;


        public DatabaseFixture()
        {
            string host = Environment.GetEnvironmentVariable("ASPNETCORE_DB_HOST");
            string port = Environment.GetEnvironmentVariable("ASPNETCORE_DB_PORT");
            string user = Environment.GetEnvironmentVariable("ASPNETCORE_DB_USER");
            string password = Environment.GetEnvironmentVariable("ASPNETCORE_DB_PASSWORD");
            string name = Environment.GetEnvironmentVariable("ASPNETCORE_DB_NAME");

            _connectionString = General.GetConnectionString(host, name, user, password);

            dbContextOptions = new DbContextOptionsBuilder<CloudMusicContext>()
                .UseNpgsql(_connectionString)
                .EnableDetailedErrors(true)
                .EnableSensitiveDataLogging(true)
                .Options;
            using (var dbContext = new CloudMusicContext(this.dbContextOptions))
            {
                Console.WriteLine("");
                Console.WriteLine("Creating testing database");
                dbContext.Database.EnsureDeleted();
                dbContext.Database.Migrate();
            }

            _checkpoint = new Checkpoint
            {
                SchemasToInclude = new[]
                {
                    "public"
                },
                DbAdapter = DbAdapter.Postgres
            };
        }

        public string ConnectionString()
        {
            return _connectionString;
        }

        public async Task ResetDatabase()
        {
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                await conn.OpenAsync();
                await _checkpoint.Reset(conn);
            }
        }

        public void Dispose()
        {
            using (var dbContext = new CloudMusicContext(this.dbContextOptions))
            {
                Console.WriteLine("");
                Console.WriteLine("Deleting testing database");
                dbContext.Database.EnsureDeleted();
            }
        }
    }
    [CollectionDefinition("Database collection")]
    public class DatabaseCollection : ICollectionFixture<DatabaseFixture>
    { }
}