using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ThirdParty.Json.LitJson;

namespace CloudMusicTest.E2E
{
    public static class E2ETestHelper
    {
        public static async Task<HttpResponseMessage> CreateAccount(HttpClient client, dynamic accountParams)
        {
            var json = JsonMapper.ToJson(accountParams);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/auth/signup/", stringContent);
            return response;
        }

        public static async Task<HttpResponseMessage> Login(HttpClient client, dynamic loginParams)
        {
            var json = JsonMapper.ToJson(loginParams);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/auth/login/", stringContent);
            return response;
        }

        public static async Task<HttpResponseMessage> Logout(HttpClient client)
        {
            var stringContent = new StringContent("", UnicodeEncoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/auth/logout/", stringContent);
            return response;
        }
        public static async Task SignupLoginFlow(HttpClient client)
        {
            var responseCreateAccount = await CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse"
            });

            var responseLogin = await Login(client, new
            {
                email = "test@mail.com",
                password = "test123",
            });
        }
    }
}