using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace CloudMusicTest.E2E
{
    [Collection("Database collection")]
    public class UploadFileTest : IClassFixture<WebApplicationFactory<CloudMusic.Startup>>
    {
        private readonly WebApplicationFactory<CloudMusic.Startup> _factory;
        private readonly DatabaseFixture _dbFixture;
        private readonly DirectoryInfo _currentPath;
        public UploadFileTest(WebApplicationFactory<CloudMusic.Startup> factory, DatabaseFixture fixture)
        {
            _factory = factory;
            _dbFixture = fixture;
            _currentPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent;
        }


        [Fact]
        public async Task TestUploadFile()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            await E2ETestHelper.SignupLoginFlow(client);

            using (var formContent = new MultipartFormDataContent())
            {
                formContent.Headers.ContentType.MediaType = "multipart/form-data";
                Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/test.mp3"));
                formContent.Add(new StreamContent(fileStream), "file", "test.mp3");

                var response = await client.PostAsync("/api/upload_song/", formContent);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task TestEmptyFile()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            await E2ETestHelper.SignupLoginFlow(client);

            using (var formContent = new MultipartFormDataContent())
            {
                formContent.Headers.ContentType.MediaType = "multipart/form-data";
                Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/empty.mp3"));
                formContent.Add(new StreamContent(fileStream), "file", "empty.mp3");

                var response = await client.PostAsync("/api/upload_song/", formContent);
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
                var responseData = await response.Content.ReadAsStringAsync();

                Assert.Contains("empty request", responseData);
            }
        }
        [Fact]
        public async Task TestEmptyRequest()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            await E2ETestHelper.SignupLoginFlow(client);

            using (var formContent = new MultipartFormDataContent())
            {
                formContent.Headers.ContentType.MediaType = "multipart/form-data";
                var response = await client.PostAsync("/api/upload_song/", formContent);
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }
    }
}
