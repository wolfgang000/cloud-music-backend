using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using ThirdParty.Json.LitJson;
using System.Linq;
using Xunit;
using System.Web;

namespace CloudMusicTest.E2E
{
    [Collection("Database collection")]
    public class AuthenticationTest : IClassFixture<WebApplicationFactory<CloudMusic.Startup>>
    {
        private readonly WebApplicationFactory<CloudMusic.Startup> _factory;
        private readonly DatabaseFixture _dbFixture;
        public AuthenticationTest(WebApplicationFactory<CloudMusic.Startup> factory, DatabaseFixture fixture)
        {
            _factory = factory;
            _dbFixture = fixture;
        }

        private string GetCookie(HttpResponseMessage message)
        {
            message.Headers.TryGetValues("Set-Cookie", out var setCookie);
            var setCookieString = setCookie.Single();
            var cookieTokens = setCookieString.Split(';');
            var firstCookie = cookieTokens.FirstOrDefault();
            var keyValueTokens = firstCookie.Split('=');
            var valueString = keyValueTokens[1];
            var cookieValue = HttpUtility.UrlDecode(valueString);
            return cookieValue;
        }

        [Fact]
        public async Task TestCreateAccount()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            var responseCreateAccount = await E2ETestHelper.CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse",
            });
            Assert.Equal(HttpStatusCode.Created, responseCreateAccount.StatusCode);
        }

        [Fact]
        public async Task TestTryCreateMultipleAccountsWithTheSameEmail()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            var response1 = await E2ETestHelper.CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse"
            });
            Assert.Equal(HttpStatusCode.Created, response1.StatusCode);

            var response2 = await E2ETestHelper.CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse"
            });
            Assert.Equal(HttpStatusCode.BadRequest, response2.StatusCode);
        }


        [Fact]
        public async Task TestLogin()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            var response = await E2ETestHelper.CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse"
            });
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var responseLogin = await E2ETestHelper.Login(client, new
            {
                email = "test@mail.com",
                password = "test123",
            });
            Assert.Equal(HttpStatusCode.OK, responseLogin.StatusCode);
            responseLogin.Headers.TryGetValues("Set-Cookie", out var setCookie);
            var setCookieString = setCookie.Single();
            Assert.False(String.IsNullOrWhiteSpace(setCookieString));
            Assert.Contains("AspNetCore.Cookies", setCookieString);
        }


        [Fact]
        public async Task TestLogout()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            await E2ETestHelper.SignupLoginFlow(client);

            var responseLogut = await E2ETestHelper.Logout(client);
            Assert.Equal(HttpStatusCode.NoContent, responseLogut.StatusCode);
            responseLogut.Headers.TryGetValues("Set-Cookie", out var setCookie);
            var setCookieString = setCookie.Single();
            Console.WriteLine(setCookieString);
            Assert.False(String.IsNullOrWhiteSpace(setCookieString));
            Assert.Contains("AspNetCore.Cookies=;", setCookieString);
        }

        [Fact]
        public async Task TestTryLoginWithWrongParams()
        {
            await _dbFixture.ResetDatabase();
            var client = _factory.CreateClient();
            var response = await E2ETestHelper.CreateAccount(client, new
            {
                email = "test@mail.com",
                password = "test123",
                name = "wilson",
                betaCode = "dummycodedonotuse"
            });
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var responseLogin = await E2ETestHelper.Login(client, new
            {
                email = "test@mail.com",
                password = "test12ss",
            });
            Assert.Equal(HttpStatusCode.BadRequest, responseLogin.StatusCode);
        }
    }
}
