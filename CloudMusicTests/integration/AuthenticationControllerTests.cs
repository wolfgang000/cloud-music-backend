using System;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using CloudMusic.Views;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using CloudMusicTest.Mocks;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class AuthenticationControllerTests : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly CloudMusicContext _dbContext;

        private readonly UserService _userService;
        private readonly AuthenticationController _controller;

        public AuthenticationControllerTests(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._userService = new UserService(this._dbContext);
            this._controller = new AuthenticationController(null, this._userService);
        }
        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        [Fact]
        public async Task TestTryCreateAccountWithRepeatedEmail()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._userService.Create(new User
            {
                Email = "test@mail.com",
                Name = "Testing",
            }, "test123");
            Assert.True(userResult.IsSuccess);

            var badPyaload = new SignupRequest
            {
                Email = "test@mail.com",
                Name = "Testing",
                BetaCode = "dummycodedonotuse",
                Password = "thet"
            };
            var result1 = await _controller.CreateAccount(badPyaload);
            var badRequest1 = Assert.IsType<BadRequestObjectResult>(result1);
            var returnValue1 = Assert.IsType<CloudMusic.Errors.ErrorResponse>(badRequest1.Value);
            Assert.Contains("already taken", returnValue1.Errors.GetValueOrDefault("Email")[0]);

            var badPyaloadUpperCase = new SignupRequest
            {
                Email = "Test@Mail.com",
                Name = "Testing",
                BetaCode = "dummycodedonotuse",
                Password = "thet"
            };
            var result2 = await _controller.CreateAccount(badPyaloadUpperCase);
            var badRequest2 = Assert.IsType<BadRequestObjectResult>(result2);
            var returnValue2 = Assert.IsType<CloudMusic.Errors.ErrorResponse>(badRequest2.Value);
            Assert.Contains("already taken", returnValue2.Errors.GetValueOrDefault("Email")[0]);
        }

        [Fact]
        public async Task TestTryCreateAccountWithBadEmail()
        {
            await _dbFixture.ResetDatabase();

            var badPyaload = new SignupRequest
            {
                Email = "test",
                Name = "Testing",
                BetaCode = "dummycodedonotuse",
                Password = "thet"
            };

            var result = await _controller.CreateAccount(badPyaload);
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnValue = Assert.IsType<CloudMusic.Errors.ErrorResponse>(badRequest.Value);
            Assert.Contains("not a valid email address", returnValue.Errors.GetValueOrDefault("Email")[0]);
        }

        [Fact]
        public async Task TestGetUserDetail()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._userService.Create(ModelFactory.User().Build(), "test133");
            var user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new MockHttpContext(new ClaimsPrincipal(claimsIdentity));

            var result = await _controller.GetUserDetail();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<UserDetailView>(okResult.Value);

            Assert.Equal(user.Email, returnValue.Email);
        }
    }
}
