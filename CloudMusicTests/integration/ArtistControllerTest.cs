using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using CloudMusic.Views;
using FizzWare.NBuilder;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using CloudMusicTest.Mocks;
using System.Linq;
using Microsoft.Extensions.Logging;
using CloudMusic.Adapters;
using CloudMusic.Models.Queries;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class ArtistControllerTest : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly CloudMusicContext _dbContext;
        private readonly RepositoryService _repo;
        private readonly ArtistController _controller;
        private User _user;

        public ArtistControllerTest(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._repo = new RepositoryService(this._dbContext);
            var logger = new LoggerFactory().CreateLogger<ArtistController>();
            var fileSystemStorage = Environment.GetEnvironmentVariable("ASPNETCORE_FILE_STORAGE_MEDIA_PATH");
            this._controller = new ArtistController(
                this._repo,
                new FileSystemStorage(fileSystemStorage, "http://localhost:5000/media/music"),
                logger
            );
        }

        private async Task Setup()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._repo.UserService.Create(ModelFactory.User().Build(), "test133");
            this._user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, this._user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new MockHttpContext(new ClaimsPrincipal(claimsIdentity));
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        [Fact]
        public async Task TestListUsersArtists()
        {
            await Setup();

            var artists = ModelFactory.ArtistList(3)
                .Build();

            foreach (var a in artists)
            {
                await this._repo.ArtistService.SaveOne(a);
            }

            var artist1 = artists.Cast<Artist>().ElementAt(0);
            var artist2 = artists.Cast<Artist>().ElementAt(1);
            var artist3 = artists.Cast<Artist>().ElementAt(2);

            var album = ModelFactory.Album()
                .With(a => a.User = _user)
                .With(a => a.Artist = artist1)
                .Build();

            await this._repo.AlbumService.SaveOne(album);

            var track = ModelFactory.Track()
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .With(a => a.Artist = artist1)
                   .Build()
               )
               .With(a => a.User = _user)
               .With(a => a.Artist = artist2)
               .Build();

            await this._repo.TrackService.SaveOne(track);

            var result = await _controller.GetArtists();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<ArtistBaseView>>(okResult.Value);

            Assert.Equal(2, returnValue.ToArray().Length);
        }

        [Fact]
        public async Task TestGetArtistWithAlbum()
        {
            await Setup();

            var artist = ModelFactory.Artist()
                .Build();

            await this._repo.ArtistService.SaveOne(artist);

            var album = ModelFactory.Album()
                .With(a => a.User = _user)
                .With(a => a.Artist = artist)
                .Build();

            await this._repo.AlbumService.SaveOne(album);

            var track = ModelFactory.Track()
                .With(t => t.Album = ModelFactory.Album()
                    .With(a => a.User = _user)
                    .Build()
                )
                .With(a => a.User = _user)
                .With(a => a.Artist = artist)
                .Build();

            await this._repo.TrackService.SaveOne(track);

            var otherAlbums = ModelFactory.AlbumList(3)
                .With(a => a.User = _user)
                .Build();

            foreach (var a in otherAlbums)
            {
                await this._repo.AlbumService.SaveOne(a);
            }

            var result = await _controller.GetArtist(artist.Id);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var artistResponse = Assert.IsType<ArtistBaseView>(okResult.Value);
        }
    }
}
