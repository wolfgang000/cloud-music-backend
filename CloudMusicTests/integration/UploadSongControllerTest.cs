using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Microsoft.AspNetCore.Http;
using CloudMusic.Adapters;
using CloudMusic.Views;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using CloudMusicTest.Mocks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class UploadSongControllerTest : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly DirectoryInfo _currentPath;
        private readonly CloudMusicContext _dbContext;
        private readonly RepositoryService _repo;
        private readonly TrackService _trackService;
        private readonly AlbumService _albumService;
        private readonly ArtistService _artistService;
        private readonly UserService _userService;
        private readonly UploadSongController _controller;
        private readonly string _tempFilesPath;
        private User _user;

        public UploadSongControllerTest(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._tempFilesPath = $"/tmp/{Guid.NewGuid().ToString()}/";
            System.IO.Directory.CreateDirectory(this._tempFilesPath);
            var generalConfig = new GeneralConfig(_tempFilesPath);
            this._currentPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._repo = new RepositoryService(this._dbContext);
            this._trackService = new TrackService(this._dbContext);
            this._albumService = new AlbumService(this._dbContext);
            this._artistService = new ArtistService(this._dbContext);
            var fileSystemStorage = Environment.GetEnvironmentVariable("ASPNETCORE_FILE_STORAGE_MEDIA_PATH");
            this._userService = new UserService(this._dbContext);
            var logger = new LoggerFactory().CreateLogger<UploadSongController>();

            this._controller = new UploadSongController(
                _repo,
                generalConfig,
                new FileSystemStorage(fileSystemStorage, "http://localhost:5000/media/music"),
                logger
            );
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        private async Task Setup()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._userService.Create(ModelFactory.User().Build(), "test133");
            this._user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, this._user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new MockHttpContext(new ClaimsPrincipal(claimsIdentity));
        }

        [Fact]
        public async Task TestUploadMp3()
        {
            await Setup();

            Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/01 - Overture.mp3"));
            FormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "file", "test.mp3");

            var result = await _controller.UploadSong(formFile);
            var createdResult = Assert.IsType<CreatedResult>(result);
            var trackResponse = (TrackResponseView)createdResult.Value;

            var newTrackResult = await _trackService.FindById(trackResponse.Id);
            Assert.True(newTrackResult.IsSuccess);
            var newTrack = newTrackResult.Value;
            Assert.Equal("audio/mpeg", newTrack.ContentType);
            Assert.Equal("Overture", newTrack.Title);
            Assert.Equal(1, newTrack.Number);
            Assert.Empty(Directory.GetFiles(_tempFilesPath));
        }

        [Fact]
        public async Task TestUploadMp3WithoutCoverArt()
        {
            await Setup();

            Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/track_without_coverart.mp3"));
            FormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "file", "test.mp3");

            var result = await _controller.UploadSong(formFile);
            var createdResult = Assert.IsType<CreatedResult>(result);
            var trackResponse = (TrackResponseView)createdResult.Value;

            var newTrackResult = await _trackService.FindById(trackResponse.Id);
            Assert.True(newTrackResult.IsSuccess);
            var newTrack = newTrackResult.Value;
            Assert.Equal("audio/mpeg", newTrack.ContentType);
            Assert.Equal("-Retry-", newTrack.Title);
            Assert.Equal(1, newTrack.Number);
            Assert.Empty(Directory.GetFiles(_tempFilesPath));
        }


        [Fact]
        public async Task TestUploadFlac()
        {
            await Setup();

            Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/Steven Universe Future Intro.flac"));
            FormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "file", "test.flac");

            var result = await _controller.UploadSong(formFile);
            var createdResult = Assert.IsType<CreatedResult>(result);
            var trackResponse = (TrackResponseView)createdResult.Value;

            var newTrackResult = await _trackService.FindById(trackResponse.Id);
            Assert.True(newTrackResult.IsSuccess);
            var newTrack = newTrackResult.Value;
            Assert.Equal("audio/flac", newTrack.ContentType);
            Assert.Equal("Steven Universe Future Intro", newTrack.Title);
            Assert.Equal(1, newTrack.Number);
            Assert.Empty(Directory.GetFiles(_tempFilesPath));
        }

        [Fact]
        public async Task TestTryUploadTrackOnLowStorageAvalable()
        {
            await Setup();
            this._user.StorageLimitSize = 1;
            var userResult = await this._userService.Update(this._user);
            this._user = userResult.Value;

            Stream fileStream = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/01 - Overture.mp3"));
            FormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "file", "test.mp3");

            var result = await _controller.UploadSong(formFile);
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnValue = Assert.IsType<CloudMusic.Errors.ErrorResponse>(badRequest.Value);
            Assert.Equal(CloudMusic.Errors.ErrorCodes.INSUFFICIENT_STORAGE_AVAILABLE.GetHashCode(), returnValue.Code);
        }


        [Fact]
        public async Task TestTryUploadRepeatedTrack()
        {
            await Setup();

            Stream fileStream1 = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/01 - Overture.mp3"));
            FormFile formFile1 = new FormFile(fileStream1, 0, fileStream1.Length, "file", "test.mp3");

            var result1 = await _controller.UploadSong(formFile1);
            Assert.IsType<CreatedResult>(result1);

            Stream fileStream2 = System.IO.File.OpenRead(Path.Join(_currentPath.ToString(), "assets/01 - Overture.mp3"));
            FormFile formFile2 = new FormFile(fileStream2, 0, fileStream2.Length, "file", "test.mp3");
            var result2 = await _controller.UploadSong(formFile2);
            var badRequest = Assert.IsType<BadRequestObjectResult>(result2);
            var returnValue = Assert.IsType<CloudMusic.Errors.ErrorResponse>(badRequest.Value);
            Assert.Equal(CloudMusic.Errors.ErrorCodes.TRACK_ALREADY_EXISTS.GetHashCode(), returnValue.Code);
        }

    }
}
