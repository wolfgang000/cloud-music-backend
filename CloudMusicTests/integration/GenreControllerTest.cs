using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using CloudMusic.Views;
using FizzWare.NBuilder;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using CloudMusicTest.Mocks;
using System.Linq;
using Microsoft.Extensions.Logging;
using CloudMusic.Adapters;
using CloudMusic.Models.Queries;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class GenreControllerTest : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly CloudMusicContext _dbContext;
        private readonly RepositoryService _repo;
        private readonly GenreController _controller;
        private User _user;

        public GenreControllerTest(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._repo = new RepositoryService(this._dbContext);
            var logger = new LoggerFactory().CreateLogger<GenreController>();
            var fileSystemStorage = Environment.GetEnvironmentVariable("ASPNETCORE_FILE_STORAGE_MEDIA_PATH");
            this._controller = new GenreController(
                this._repo,
                new FileSystemStorage(fileSystemStorage, "http://localhost:5000/media/music"),
                logger
            );
        }

        private async Task Setup()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._repo.UserService.Create(ModelFactory.User().Build(), "test133");
            this._user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, this._user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new MockHttpContext(new ClaimsPrincipal(claimsIdentity));
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        [Fact]
        public async Task TestListGenresWithAlbumsAndArtistsCounts()
        {
            await Setup();

            var genres = ModelFactory.GenreList(3)
                .Build();

            foreach (var a in genres)
            {
                await this._repo.GenreService.SaveOne(a);
            }

            var genre1 = genres.Cast<Genre>().ElementAt(0);
            var genre2 = genres.Cast<Genre>().ElementAt(1);
            var genre3 = genres.Cast<Genre>().ElementAt(2);

            var track = ModelFactory.Track()
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .With(t => t.Genre = genre1)
               .Build();

            await this._repo.TrackService.SaveOne(track);

            var otherTracks = ModelFactory.TrackList(4)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.Genre = genre2)
               .Build();

            foreach (var t in otherTracks)
            {
                await this._repo.TrackService.SaveOne(t);
            }

            var result = await _controller.GetGenres();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<GenreWithAlbumsAndArtistsCounts>>(okResult.Value);

            Assert.Equal(2, returnValue.ToArray().Length);
            var genreResult1 = returnValue.Find(g => g.Id == genre1.Id);
            Assert.Equal(1, genreResult1.TracksCount);
            Assert.Equal(1, genreResult1.AlbumsCount);

            var genreResult2 = returnValue.Find(g => g.Id == genre2.Id);
            Assert.Equal(4, genreResult2.TracksCount);
            Assert.Equal(4, genreResult2.AlbumsCount);
        }
    }
}
