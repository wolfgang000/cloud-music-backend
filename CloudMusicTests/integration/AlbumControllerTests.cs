using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using CloudMusic.Adapters;
using CloudMusic.Views;
using FizzWare.NBuilder;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class AlbumControllerTest : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly DirectoryInfo _currentPath;
        private readonly CloudMusicContext _dbContext;
        private readonly UserService _userService;
        private readonly TrackService _trackService;
        private readonly AlbumService _albumService;
        private readonly IRepositoryService _repo;
        private readonly AlbumController _controller;
        private User _user;

        public AlbumControllerTest(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._currentPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._userService = new UserService(this._dbContext);
            this._trackService = new TrackService(this._dbContext);
            this._albumService = new AlbumService(this._dbContext);
            this._repo = new RepositoryService(this._dbContext);
            var fileSystemStorage = Environment.GetEnvironmentVariable("ASPNETCORE_FILE_STORAGE_MEDIA_PATH");
            this._controller = new AlbumController(
                _albumService,
                null,
                new FileSystemStorage(fileSystemStorage, "http://localhost:5000/media/music")
            );
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        private async Task Setup()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._userService.Create(ModelFactory.User().Build(), "test133");
            this._user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, this._user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(claimsIdentity)
            };
        }

        [Fact]
        public async Task TestListAlbums()
        {
            await Setup();

            var albums = ModelFactory.AlbumList(2)
                .With(a => a.User = _user)
                .Build();

            foreach (var album in albums)
            {
                await _albumService.SaveOne(album);
            }

            var result = await _controller.GetAlbums();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<AlbumBaseView>>(okResult.Value);

            Assert.Equal(2, returnValue.ToArray().Length);
        }

        [Fact]
        public async Task TestListAlbumsSortByInsertedAt()
        {
            await Setup();

            var newAlbum = ModelFactory.Album()
                .With(a => a.User = _user)
                .With(a => a.InsertedAt = DateTime.Now)
                .Build();

            var oldAlbums = ModelFactory.AlbumList(2)
                .With(a => a.User = _user)
                .With(a => a.InsertedAt = DateTime.Now.AddHours(-2))
                .Build();

            foreach (var album in oldAlbums.Append(newAlbum))
            {
                await _albumService.SaveOne(album);
            }

            _controller.HttpContext.Request.QueryString = new QueryString($"?orderBy=InsertedAt:desc");
            var result = await _controller.GetAlbums();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<AlbumBaseView>>(okResult.Value);
            Assert.Equal(3, returnValue.ToArray().Length);
            Assert.Equal(newAlbum.Id, returnValue.First().Id);
        }

        [Fact]
        public async Task TestListAlbumsByGenre()
        {
            await Setup();

            var genre = ModelFactory.Genre().Build();
            await this._repo.GenreService.SaveOne(genre);

            var tracksWithGenre = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .With(t => t.Genre = genre)
               .Build();

            var otherTrack = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .Build();

            foreach (var t in otherTrack.Concat(tracksWithGenre))
            {
                await _repo.TrackService.SaveOne(t);
            }

            _controller.HttpContext.Request.QueryString = new QueryString($"?genreId={genre.Id}");
            var result = await _controller.GetAlbums();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<AlbumBaseView>>(okResult.Value);

            Assert.Equal(5, returnValue.ToArray().Length);
        }

        [Fact]
        public async Task TestListAlbumsByArtist()
        {
            await Setup();

            var artist = ModelFactory.Artist().Build();
            await this._repo.ArtistService.SaveOne(artist);

            var tracksWithArtist = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .With(t => t.Artist = artist)
               .Build();

            var otherTrack = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .Build();

            foreach (var t in otherTrack.Concat(tracksWithArtist))
            {
                await _repo.TrackService.SaveOne(t);
            }

            _controller.HttpContext.Request.QueryString = new QueryString($"?artistId={artist.Id}");
            var result = await _controller.GetAlbums();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<AlbumBaseView>>(okResult.Value);

            Assert.Equal(5, returnValue.ToArray().Length);
        }


        [Fact]
        public async Task TestAlbumDetail()
        {
            await Setup();

            var album = ModelFactory.Album()
               .With(a => a.User = _user)
               .Build();

            await _albumService.SaveOne(album);

            var tracks = ModelFactory.TrackList(10).All()
                .With(a => a.Album = album)
                .With(a => a.User = _user)
                .Build();

            foreach (var track in tracks)
            {
                await _trackService.SaveOne(track);
            }

            var result = await _controller.GetAlbumDetail(album.Id);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var albumResponse = Assert.IsType<AlbumDetailView>(okResult.Value);
            Assert.Equal(tracks.Count, albumResponse.Tracks.Count);
            var firstTrack = albumResponse.Tracks[0];
            Assert.Equal(album.Title, firstTrack.Album.Title);
        }
    }
}
