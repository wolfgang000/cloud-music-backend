using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CloudMusic.Models;
using CloudMusic.Services;
using CloudMusic.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Microsoft.AspNetCore.Http;
using CloudMusic.Adapters;
using CloudMusic.Views;
using FizzWare.NBuilder;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Linq;

namespace CloudMusicTest.Integration
{
    [Collection("Database collection")]
    public class TrackControllerTest : IDisposable
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly DirectoryInfo _currentPath;
        private readonly CloudMusicContext _dbContext;
        private readonly TrackController _controller;
        private readonly IRepositoryService _repo;
        private User _user;

        public TrackControllerTest(DatabaseFixture fixture)
        {
            this._dbFixture = fixture;
            this._currentPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent;
            this._dbContext = new CloudMusicContext(this._dbFixture.dbContextOptions);
            this._repo = new RepositoryService(this._dbContext);
            var fileSystemStorage = Environment.GetEnvironmentVariable("ASPNETCORE_FILE_STORAGE_MEDIA_PATH");
            this._controller = new TrackController(
                this._repo,
                null,
                new FileSystemStorage(fileSystemStorage, "http://localhost:5000/media/music")
            );
        }

        private async Task Setup()
        {
            await _dbFixture.ResetDatabase();

            var userResult = await this._repo.UserService.Create(ModelFactory.User().Build(), "test133");
            this._user = userResult.Value;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, this._user.Id.ToString()),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            this._controller.ControllerContext.HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(claimsIdentity)
            };
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }

        [Fact]
        public async Task TestListUsersTracks()
        {
            await Setup();

            var album = ModelFactory.Album()
                .With(a => a.User = _user)
                .Build();

            await _repo.AlbumService.SaveOne(album);

            var tracks = ModelFactory.TrackList(2)
                .With(t => t.Album = album)
                .With(a => a.User = _user)
                .Build();

            foreach (var track in tracks)
            {
                await _repo.TrackService.SaveOne(track);
            }

            var result = await _controller.GetTracks();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<TrackResponseView>>(okResult.Value);

            Assert.Equal(2, returnValue.ToArray().Length);
        }


        [Fact]
        public async Task TestListTracksByGenre()
        {
            await Setup();

            var genre = ModelFactory.Genre().Build();
            await this._repo.GenreService.SaveOne(genre);

            var tracksWithGenre = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .With(t => t.Genre = genre)
               .Build();

            var otherTrack = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .Build();

            foreach (var t in otherTrack.Concat(tracksWithGenre))
            {
                await _repo.TrackService.SaveOne(t);
            }

            _controller.HttpContext.Request.QueryString = new QueryString($"?genreId={genre.Id}");
            var result = await _controller.GetTracks();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<TrackResponseView>>(okResult.Value);

            Assert.Equal(5, returnValue.ToArray().Length);
        }

        [Fact]
        public async Task TestListTracksByArtist()
        {
            await Setup();

            var artist = ModelFactory.Artist().Build();
            await this._repo.ArtistService.SaveOne(artist);

            var tracksWithGenre = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .With(t => t.Artist = artist)
               .Build();

            var otherTrack = ModelFactory.TrackList(5)
               .With(t => t.Album = ModelFactory.Album()
                   .With(a => a.User = _user)
                   .Build()
               )
               .With(t => t.User = _user)
               .Build();

            foreach (var t in otherTrack.Concat(tracksWithGenre))
            {
                await _repo.TrackService.SaveOne(t);
            }

            _controller.HttpContext.Request.QueryString = new QueryString($"?artistId={artist.Id}");
            var result = await _controller.GetTracks();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<TrackResponseView>>(okResult.Value);

            Assert.Equal(5, returnValue.ToArray().Length);
        }
    }
}
