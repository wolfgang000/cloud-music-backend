using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace CloudMusic.Utils
{
    public static class General
    {
        public static bool IsNullOrEmpty<T>(this T[] array)
        {
            return array == null || array.Length == 0;
        }
        public static bool IsNullOrEmpty<T>(this List<T> list)
        {
            return list == null || list.Count == 0;
        }
        public static string StringFirstLower(string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            return s[0].ToString().ToLower() + s.Substring(1);
        }
        public static string GetStringOrUnknown(string s)
        {
            return string.IsNullOrWhiteSpace(s) ? "Unknown" : s.Trim();
        }
        public static string GetConnectionString(string host, string dbname, string username, string password)
        {
            return $"Host={host};Database={dbname};Username={username};Password={password}";
        }
        public static string GetConfigurationValue(IConfiguration configuration, string key)
        {
            var value = configuration.GetValue<string>(key);
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception($"'{key}' property not defined");
            }
            return value;
        }
        public static Dictionary<string, string[]> FormatFluentValidationErrorsIntoDictionary(IList<FluentValidation.Results.ValidationFailure> errors)
        {
            var errorMap = new Dictionary<string, string[]>();
            foreach (var failure in errors)
            {
                if (errorMap.TryGetValue(failure.PropertyName, out var oldErrors))
                {
                    errorMap[failure.PropertyName] = oldErrors.Append(failure.ErrorMessage).ToArray();
                }
                else
                {
                    string[] newErrors = { failure.ErrorMessage };
                    errorMap.Add(failure.PropertyName, newErrors);
                }
            }
            return errorMap;
        }

        public static long GetUserId(HttpContext httpContext)
        {
            var userIdStr = (from c in httpContext.User.Claims
                             where c.Type == ClaimTypes.NameIdentifier
                             select c.Value).FirstOrDefault();
            return long.Parse(userIdStr);
        }
    }
}