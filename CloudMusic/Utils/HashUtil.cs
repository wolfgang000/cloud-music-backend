using System.IO;
using System;
using System.Security.Cryptography;

// TODO: refactor
// https://lonewolfonline.net/calculate-md5-checksum-file/
// https://stackoverflow.com/questions/10520048/calculate-md5-checksum-for-a-file

namespace CloudMusic.Utils
{
    public class HashUtil
    {
        public static string GetSHA1HashFromFile(string fileName)
        {
            using (var hashAlgorithm = SHA1.Create())
            {
                using (var stream = new BufferedStream(File.OpenRead(fileName), 100000))
                {
                    return ByteArrayToString(hashAlgorithm.ComputeHash(stream));
                }
            }
        }

        public static string ByteArrayToString(byte[] data)
        {
            return BitConverter.ToString(data).Replace("-", "").ToLowerInvariant();
        }
    }
}