using System;
using System.Collections.Generic;
using System.Linq;
using CloudMusic.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using CloudMusic.Adapters;
using CSharpFunctionalExtensions;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using CloudMusic.Models;
using CloudMusic.Errors;
using Microsoft.Extensions.Logging;

namespace CloudMusic.Utils
{
    using SuccessMap = Dictionary<string, dynamic>;

    using ResultType = Result<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>;
    public static class TrackUploaderUtil
    {
        private static readonly string[] allowFileTypes = { ".mp3", ".flac" };

        public static void CleanUpTempFiles(SuccessMap accMap)
        {
            if (accMap.TryGetValue("tempFilePath", out var tempFilePath) && System.IO.File.Exists(tempFilePath))
            {
                System.IO.File.Delete(tempFilePath);
            }
            if (accMap.TryGetValue("albumTempFilePath", out var albumTempFilePath) && System.IO.File.Exists(albumTempFilePath))
            {
                System.IO.File.Delete(albumTempFilePath);
            }
        }

        public static async Task DeleteObjectStorageFiles(SuccessMap accMap, IObjectStorage objectStorage)
        {
            if (accMap.TryGetValue("trackObjectKey", out var trackObjectKey))
            {
                await objectStorage.DeleteAsync(trackObjectKey);
            }
            if (accMap.TryGetValue("albumCoverArtObjectStorageKey", out var albumCoverArtObjectStorageKey))
            {
                await objectStorage.DeleteAsync(albumCoverArtObjectStorageKey);
            }
        }

        public static async Task<ResultType> ValidateUserAvailableStorage(SuccessMap accMap, IRepositoryService repo)
        {
            IFormFile file = accMap["file"];
            long userId = accMap["userId"];
            User user = accMap["user"];

            var usedStorage = await repo.UserService.GetUsedStorageSize(userId);
            if (file.Length <= 0)
                return Result.Failure<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("empty request")));
            else if ((file.Length + usedStorage) > user.StorageLimitSize)
                return Result.Failure<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult(
                    new ErrorResponse
                    {
                        Code = ErrorCodes.INSUFFICIENT_STORAGE_AVAILABLE.GetHashCode(),
                        Message = ErrorHelper.GetMessage(ErrorCodes.INSUFFICIENT_STORAGE_AVAILABLE),
                    }
                )));
            else
                return Result.Success<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>(accMap);
        }

        public static ResultType ValidateFileExtension(SuccessMap accMap)
        {
            IFormFile file = accMap["file"];
            var fileExtension = Path.GetExtension(file.FileName);

            if (!allowFileTypes.Contains(fileExtension))
                return Result.Failure<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("file format not suported")));
            else
            {
                accMap.Add("fileExtension", fileExtension);
                return Result.Success<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>(accMap);
            }
        }

        public static async Task<ResultType> SaveTempFile(SuccessMap accMap, string tempFilesPath)
        {
            IFormFile file = accMap["file"];
            string fileExtension = accMap["fileExtension"];
            var tempFileName = $"{Guid.NewGuid().ToString()}{fileExtension}";
            var tempFilePath = Path.Join(tempFilesPath, tempFileName);
            using (var stream = new FileStream(tempFilePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            var sha1Hash = HashUtil.GetSHA1HashFromFile(tempFilePath);

            accMap.Add("tempFileName", tempFileName);
            accMap.Add("tempFilePath", tempFilePath);
            accMap.Add("sha1Hash", sha1Hash);

            return Result.Success<SuccessMap, (SuccessMap, IActionResult ErrorResponse)>(accMap);
        }

        public static async Task<ResultType> ValidateIfTrackAlreadyExists(SuccessMap accMap, IRepositoryService repo)
        {
            string sha1Hash = accMap["sha1Hash"];
            IFormFile file = accMap["file"];
            long userId = accMap["userId"];
            var trackResult = await repo.TrackService.FindByHashAndUserId(sha1Hash, userId);
            if (trackResult.IsSuccess)
            {
                return Result.Failure<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult(
                    new ErrorResponse
                    {
                        Code = ErrorCodes.TRACK_ALREADY_EXISTS.GetHashCode(),
                        Message = $"{ErrorHelper.GetMessage(ErrorCodes.TRACK_ALREADY_EXISTS)}:{file.FileName}",
                    }
                )));
            }
            return Result.Success<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>(accMap);
        }


        public static async Task<ResultType> ExtractTrackMetaData(SuccessMap accMap, IRepositoryService repo, string tempFilesPath)
        {
            string tempFilePath = accMap["tempFilePath"];
            using var tfile = TagLib.File.Create(tempFilePath);
            var trackTitle = tfile.Tag.Title.Trim();
            var trackDurationSeconds = tfile.Properties.Duration.TotalSeconds;
            var trackNumber = (short)tfile.Tag.Track;
            var trackArtistName = General.GetStringOrUnknown(tfile.Tag.FirstPerformer);
            var trackGenreName = General.GetStringOrUnknown(tfile.Tag.FirstGenre);

            accMap.Add("trackTitle", trackTitle);
            accMap.Add("trackDurationSeconds", trackDurationSeconds);
            accMap.Add("trackNumber", trackNumber);
            accMap.Add("trackArtistName", trackArtistName);
            accMap.Add("trackGenreName", trackGenreName);

            var albumTitle = General.GetStringOrUnknown(tfile.Tag.Album);
            var albumArtist = General.GetStringOrUnknown(tfile.Tag.FirstAlbumArtist);

            var albumResult = await repo.AlbumService.FindByTitleAndArtist(albumTitle, albumArtist);
            if (albumResult.IsSuccess)
                accMap.Add("album", albumResult.Value);
            else
            {
                var albumYear = (ushort)tfile.Tag.Year;

                accMap.Add("albumTitle", albumTitle);
                accMap.Add("albumArtist", albumArtist);
                accMap.Add("albumYear", albumYear);
                if (tfile.Tag.Pictures.Count() >= 1)
                {
                    var picture = tfile.Tag.Pictures[0];
                    var pictureExtension = HeyRed.Mime.MimeTypesMap.GetExtension(picture.MimeType);
                    var fileName = $"{Guid.NewGuid().ToString()}.{pictureExtension}";
                    var albumTempFilePath = Path.Join(tempFilesPath, fileName);
                    System.IO.File.WriteAllBytes(albumTempFilePath, picture.Data.Data);

                    accMap.Add("albumTempFileName", fileName);
                    accMap.Add("albumTempFilePath", albumTempFilePath);
                    accMap.Add("albumCoverArtContentType", picture.MimeType);
                    accMap.Add("albumCoverArtSize", picture.Data.Data.Length);
                }

            }
            return Result.Success<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>(accMap);
        }

        public static async Task<ResultType> UploadFile(SuccessMap accMap, IObjectStorage objectStorage)
        {
            // Upload Track
            //======================================//
            long userId = accMap["userId"];
            string fileExtension = accMap["fileExtension"];
            string tempFileName = accMap["tempFileName"];
            string tempFilePath = accMap["tempFilePath"];
            var trackObjectKey = $"{userId}/music/{tempFileName}";
            var fileContentType = MimeTypeMap.List.MimeTypeMap.GetMimeType(fileExtension).ToArray()[0];
            await objectStorage.UploadAsync(tempFilePath, trackObjectKey, fileContentType);
            accMap.Add("trackObjectKey", trackObjectKey);
            accMap.Add("fileContentType", fileContentType);

            // Upload Album Cover
            //======================================//
            if (!accMap.ContainsKey("album") && accMap.TryGetValue("albumTempFilePath", out var albumTempFilePath))
            {
                var albumTempFileName = accMap["albumTempFileName"];
                var albumCoverArtContentType = accMap["albumCoverArtContentType"];
                var objectKey = $"{userId}/covers/{albumTempFileName}";
                await objectStorage.UploadAsync(albumTempFilePath, objectKey, albumCoverArtContentType);
                accMap.Add("albumCoverArtObjectStorageKey", objectKey);
            }

            return Result.Success<SuccessMap, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>(accMap);
        }

        public static async Task<ResultType> SaveEntities<T>(SuccessMap accMap, IRepositoryService repo, ILogger<T> logger)
        {
            IFormFile file = accMap["file"];
            long userId = accMap["userId"];
            string trackObjectKey = accMap["trackObjectKey"];
            string fileContentType = accMap["fileContentType"];
            string sha1Hash = accMap["sha1Hash"];

            // Get or create Track Artist
            //======================================//
            string trackArtistName = accMap["trackArtistName"];
            string trackGenreName = accMap["trackGenreName"];
            using (var transaction = repo.DbContext.Database.BeginTransaction())
            {
                // Get or create Track Artist
                //======================================//
                var artistResult = await repo.ArtistService.GetByNameOrCreate(trackArtistName);
                if (artistResult.IsFailure)
                {
                    logger.LogError($"Artist could not be created with name: ${trackArtistName}, this should not have happened");
                    logger.LogError(artistResult.Error);
                    return Result.Failure<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("Error while track")));
                }
                var trackArtist = artistResult.Value;
                accMap.Add("trackArtist", trackArtist);

                // Get or create Track Genre
                //======================================//
                var genreResult = await repo.GenreService.GetByNameOrCreate(trackGenreName);
                if (genreResult.IsFailure)
                {
                    logger.LogError($"Genre could not be created with name: ${trackGenreName}, this should not have happened");
                    logger.LogError(genreResult.Error);
                    return Result.Failure<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("Error while track")));
                }
                var trackGenre = genreResult.Value;
                accMap.Add("trackGenre", trackGenre);

                // Get or create Album
                //======================================//
                Album album = null;
                if (accMap.TryGetValue("album", out var tempFilePath))
                {
                    album = tempFilePath;
                }
                else
                {
                    string albumArtistName = accMap["albumArtist"];
                    var albumArtistResult = await repo.ArtistService.GetByNameOrCreate(albumArtistName);
                    if (albumArtistResult.IsFailure)
                    {
                        logger.LogError($"Artist could not be created with name: ${trackArtistName}, this should not have happened");
                        logger.LogError(albumArtistResult.Error);
                        return Result.Failure<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("Error while track")));
                    }
                    var albumArtist = albumArtistResult.Value;

                    album = new Album
                    {
                        Title = accMap["albumTitle"],
                        UserId = userId,
                        Year = accMap["albumYear"],
                        ArtistId = albumArtist.Id,
                        CoverArtObjectStorageKey = accMap.GetValueOrDefault("albumCoverArtObjectStorageKey", ""),
                        CoverArtContentType = accMap.GetValueOrDefault("albumCoverArtContentType", ""),
                        Size = accMap.GetValueOrDefault("albumCoverArtSize", 0),
                    };

                    var albumResult = await repo.AlbumService.SaveOne(album);

                    if (albumResult.IsFailure)
                    {
                        logger.LogError($"Album could not be created, this should not have happened");
                        logger.LogError(albumResult.Error);
                        return Result.Failure<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("Error while track")));
                    }
                    album = albumResult.Value;
                    album.Artist = albumArtist;
                    accMap.Add("album", album);
                }

                // Create Track
                //======================================//
                var track = new Track
                {
                    Title = string.IsNullOrEmpty(accMap["trackTitle"]) ? file.FileName : accMap["trackTitle"],
                    DurationSeconds = accMap["trackDurationSeconds"],
                    Number = accMap["trackNumber"],
                    OriginalFileName = file.FileName,
                    ObjectStorageKey = trackObjectKey,
                    SHA1Hash = sha1Hash,
                    Size = file.Length,
                    ContentType = fileContentType,
                    AlbumId = album.Id,
                    ArtistId = trackArtist.Id,
                    GenreId = trackGenre.Id,
                    UserId = userId
                };

                var trackResult = await repo.TrackService.SaveOne(track);
                if (trackResult.IsFailure)
                {
                    logger.LogError($"Track could not be created, this should not have happened");
                    logger.LogError(trackResult.Error);
                    return Result.Failure<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>((accMap, new BadRequestObjectResult("Error while track")));
                }
                transaction.Commit();
                accMap.Add("track", trackResult.Value);
                return Result.Success<SuccessMap, (SuccessMap AccMap, IActionResult ErrorResponse)>(accMap);
            }
        }
    }
}