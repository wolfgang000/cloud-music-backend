using System.IO;
using System;
using System.Security.Cryptography;
using System.Text;
using CloudMusic.Models;

namespace CloudMusic.Utils
{
    public class FileMetaUtil
    {
        public static Track GetFileMetadata(string filename, Track track)
        {
            using var tfile = TagLib.File.Create(filename);
            track.Title = tfile.Tag.Title.Trim();
            track.DurationSeconds = tfile.Properties.Duration.TotalSeconds;
            track.Number = (short)tfile.Tag.Track;

            return track;
        }
    }
}