using System;
using System.Linq;
using CloudMusic.Models;

namespace CloudMusic.Utils.QueryExtensions
{
    public static class AlbumQueryExtensions
    {
        public static IQueryable<Album> SortBy(this IQueryable<Album> q, string fieldStr)
        {
            String[] strlist = fieldStr.Split(":", StringSplitOptions.RemoveEmptyEntries);
            var field = strlist.DefaultIfEmpty("InsertedAt").FirstOrDefault();
            var sortDirection = strlist.DefaultIfEmpty("asc").LastOrDefault();

            return (field, sortDirection) switch
            {
                ("InsertedAt", "asc") => q.OrderBy(a => a.InsertedAt),
                ("InsertedAt", "desc") => q.OrderByDescending(a => a.InsertedAt),
                _ => q
            };
        }
    }
}