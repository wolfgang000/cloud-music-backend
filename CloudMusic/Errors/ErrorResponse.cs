using System.Collections.Generic;

namespace CloudMusic.Errors
{
    public class ErrorResponse
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public Dictionary<string, string[]> Errors { get; set; }

        public override string ToString()
        {
            return $@"[ErrorResponse (
                Code: {Code}, 
                Message: {Message},
            )]";
        }

    }
}