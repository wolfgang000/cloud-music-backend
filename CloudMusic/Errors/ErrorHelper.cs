using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CloudMusic.Errors
{
    public enum ErrorCodes
    {
        TRACK_ALREADY_EXISTS = 1,
        INSUFFICIENT_STORAGE_AVAILABLE = 2

    }

    static class ErrorHelper
    {
        private static readonly ReadOnlyDictionary<ErrorCodes, string> _dict = new ReadOnlyDictionary<ErrorCodes, string>(
            new Dictionary<ErrorCodes, string>
            {
                {ErrorCodes.TRACK_ALREADY_EXISTS, "This track has already been uploaded"},
                {ErrorCodes.INSUFFICIENT_STORAGE_AVAILABLE, "Not enough space to upload this track"},
            });
        public static string GetMessage(ErrorCodes code)
        {
            return _dict.GetValueOrDefault(code);
        }
    }
}