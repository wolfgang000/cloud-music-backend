using System;
using CloudMusic.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using CloudMusic.Adapters;
using CloudMusic.Utils;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace CloudMusic
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMemoryCache();
            ConfigureHealthCheckServices(services);
            ConfigureDbContextServices(services);
            ConfigureAuthenticationServices(services);
            ConfigureObjectStorageServices(services);
            services.AddTransient<ITrackService, TrackService>();
            services.AddTransient<IAlbumService, AlbumService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IArtistService, ArtistService>();
            services.AddTransient<IRepositoryService, RepositoryService>();
            services.AddSingleton<IGeneralConfig>(new GeneralConfig(
                General.GetConfigurationValue(Configuration, "TEMP_FILES_PATH")
            ));
        }

        public void ConfigureHealthCheckServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddNpgSql(GetConnectionString(),
                   healthQuery: "SELECT 1;",
                   name: "Postgresql",
                   failureStatus: HealthStatus.Degraded);
        }

        public void ConfigureDbContextServices(IServiceCollection services)
        {
            string connectionString = GetConnectionString();
            services.AddDbContext<Models.CloudMusicContext>(opt =>
            opt.UseNpgsql(connectionString));
        }

        private string GetConnectionString()
        {
            string host = General.GetConfigurationValue(Configuration, "DB_HOST");
            string port = General.GetConfigurationValue(Configuration, "DB_PORT");
            string user = General.GetConfigurationValue(Configuration, "DB_USER");
            string password = General.GetConfigurationValue(Configuration, "DB_PASSWORD");
            string name = General.GetConfigurationValue(Configuration, "DB_NAME");
            string connectionString = General.GetConnectionString(host, name, user, password);
            return connectionString;
        }

        public void ConfigureAuthenticationServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie("Cookies", o =>
                {
                    o.Cookie.HttpOnly = true;
                    o.Events = new CookieAuthenticationEvents
                    {
                        OnRedirectToLogin = redirectContext =>
                        {
                            redirectContext.HttpContext.Response.StatusCode = 401;
                            return Task.CompletedTask;
                        }
                    };
                });
        }


        public void ConfigureObjectStorageServices(IServiceCollection services)
        {
            var objectStorageProvider = General.GetConfigurationValue(Configuration, "OBJECT_STORAGE_PROVIDER");
            if (objectStorageProvider == "b2")
            {
                var keyId = General.GetConfigurationValue(Configuration, "B2_KEY_ID");
                var appKey = General.GetConfigurationValue(Configuration, "B2_APPLICATION_KEY");
                var bucketId = General.GetConfigurationValue(Configuration, "B2_BUCKET_ID");
                services.AddSingleton<IObjectStorage, B2Storage>();
            }
            else
            {
                services.AddSingleton<IObjectStorage>(new FileSystemStorage(
                    General.GetConfigurationValue(Configuration, "FILE_STORAGE_MEDIA_PATH"),
                    General.GetConfigurationValue(Configuration, "FILE_STORAGE_MEDIA_URL")
                    )
                );
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            ILogger<Startup> logger
        )
        {
            if (env.IsDevelopment())
            {
                logger.LogInformation("Running in Development Mode");
                app.UseDeveloperExceptionPage();

                var provider = new FileExtensionContentTypeProvider();
                provider.Mappings[".flac"] = "audio/flac";

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(General.GetConfigurationValue(Configuration, "FILE_STORAGE_MEDIA_PATH")),
                    RequestPath = new PathString("/media"),
                    ContentTypeProvider = provider
                });
            }
            else if (env.IsProduction())
            {
                logger.LogInformation($"Running in Production Mode");
            }
            else
            {
                logger.LogInformation($"Running in {env.EnvironmentName} Mode");
            }
            app.ApplicationServices.GetService<IObjectStorage>();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseHealthChecks("/api/healthcheck");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
