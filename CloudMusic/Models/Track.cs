using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FluentValidation;


namespace CloudMusic.Models
{
    public class Track
    {
        public long Id { get; set; }

        [Required]
        public string OriginalFileName { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public double DurationSeconds { get; set; }

        [Required]
        public string ObjectStorageKey { get; set; }

        [Required]
        [MaxLength(40)]
        public string SHA1Hash { get; set; }

        [Required]
        public long Size { get; set; }

        [Required]
        public string ContentType { get; set; }

        public short Number { get; set; }

        public long AlbumId { get; set; }
        public Album Album { get; set; }

        public long ArtistId { get; set; }
        public Artist Artist { get; set; }

        public long GenreId { get; set; }
        public Genre Genre { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        // TODO: https://stackoverflow.com/a/55645134
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }

        // [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }

        public override string ToString()
        {
            return $@"[Song (
                Id: {Id}, 
                Title: {Title},
                OriginalFileName: {OriginalFileName},
                ObjectStorageKey: {ObjectStorageKey},
                DurationSeconds: {DurationSeconds}
            )]";
        }

    }

    public class TrackValidator : AbstractValidator<Track>
    {
        public TrackValidator()
        {
            RuleFor(track => track.OriginalFileName).NotNull().NotEmpty();
            RuleFor(track => track.Title).NotNull().NotEmpty();
            RuleFor(track => track.DurationSeconds).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(track => track.SHA1Hash).NotNull().NotEmpty().Length(32, 32);
        }
    }
}