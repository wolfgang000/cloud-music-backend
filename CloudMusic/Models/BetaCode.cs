using System.ComponentModel.DataAnnotations;

namespace CloudMusic.Models
{
    public class BetaCode
    {
        public long Id { get; set; }

        [Required]
        public string Code { get; set; }

        public override string ToString()
        {
            return $@"[BetaCode (
                Id: {Id}, 
                Code: {Code}
            )]";
        }

    }
}