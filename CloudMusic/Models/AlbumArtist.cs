namespace CloudMusic.Models
{
    public class AlbumArtist
    {
        public string ArtistName { get; set; }
        public long AlbumId { get; set; }
        public Album Album { get; set; }
        public long ArtistId { get; set; }
        public Artist Artist { get; set; }
    }
}