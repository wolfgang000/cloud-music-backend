using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FluentValidation;


namespace CloudMusic.Models
{
    public class User
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        [Required]
        public long StorageLimitSize { get; set; }

        // TODO: https://stackoverflow.com/a/55645134
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }

        public override string ToString()
        {
            return $@"[User (
                Id: {Id}, 
                Email: {Email}
            )]";
        }

    }

    public class UserValidatorDefault : AbstractValidator<User>
    {
        public UserValidatorDefault()
        {
            RuleFor(user => user.Email).NotEmpty().EmailAddress();
            RuleFor(user => user.Name).NotEmpty();
        }
    }
}