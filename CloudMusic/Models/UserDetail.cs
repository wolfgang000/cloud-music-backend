using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FluentValidation;


namespace CloudMusic.Models
{
    public class UserDetail
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public long StorageLimitSize { get; set; }
        public long UsedStorage { get; set; }

        // TODO: https://stackoverflow.com/a/55645134
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }


        public override string ToString()
        {
            return $@"[User (
                Id: {Id}, 
                Email: {Email}
            )]";
        }

    }
}