using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation;


namespace CloudMusic.Models
{
    public class Genre
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }
        public DateTime InsertedAt { get; set; }
        public List<Track> Tracks { get; set; }


        public override string ToString()
        {
            return $@"[Genre (
                Id: {Id}, 
                Name: {Name},
            )]";
        }
    }
}