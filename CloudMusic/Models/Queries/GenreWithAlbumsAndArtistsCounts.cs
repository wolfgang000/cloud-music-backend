namespace CloudMusic.Models.Queries
{
    public class GenreWithAlbumsAndArtistsCounts
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long TracksCount { get; set; }
        public long AlbumsCount { get; set; }
    }
}