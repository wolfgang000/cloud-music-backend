using EntityFramework.Exceptions.PostgreSQL;
using Microsoft.EntityFrameworkCore;

namespace CloudMusic.Models
{
    public class CloudMusicContext : DbContext
    {
        public CloudMusicContext(DbContextOptions<CloudMusicContext> options)
            : base(options)
        { }

        public DbSet<Track> Tracks { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<BetaCode> BetaCodes { get; set; }
        public virtual DbSet<UserDetail> UsersDetail { get; set; }
        public DbSet<AlbumArtist> AlbumArtist { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseExceptionProcessor();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            builder.Entity<Artist>()
                .HasIndex(a => a.Name)
                .IsUnique();

            builder.Entity<Genre>()
                .HasIndex(a => a.Name)
                .IsUnique();

            builder
                .Entity<UserDetail>(eb =>
                {
                    eb.ToView("UsersDetailView");
                });

            builder
                .Entity<AlbumArtist>(eb =>
                {
                    eb.HasKey(a => new { a.AlbumId, a.ArtistId });

                    eb.ToView("AlbumArtistView")
                       .HasOne(a => a.Album)
                       .WithMany(a => a.Artists)
                       .HasForeignKey(a => a.AlbumId);
                });

            builder.Entity<Track>()
                .HasAlternateKey(t => new { t.SHA1Hash, t.UserId });
        }
    }
}