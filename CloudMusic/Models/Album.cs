using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation;


namespace CloudMusic.Models
{
    public class Album
    {
        public long Id { get; set; }

        [Required]
        public string Title { get; set; }

        public long ArtistId { get; set; }

        public Artist Artist { get; set; }

        public List<AlbumArtist> Artists { get; set; }

        public string CoverArtContentType { get; set; }

        public string CoverArtObjectStorageKey { get; set; }

        public long Size { get; set; }

        public ushort Year { get; set; }

        public long UserId { get; set; }

        public User User { get; set; }

        // TODO: https://stackoverflow.com/a/55645134
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }

        public List<Track> Tracks { get; set; }

        public override string ToString()
        {
            return $@"[Album (
                Id: {Id}, 
                Title: {Title},
                Artist: {Artist},
                UserId: {UserId}
            )]";
        }

    }

    public class AlbumValidator : AbstractValidator<Album>
    {
        public AlbumValidator()
        {
            RuleFor(album => album.Title).NotNull().NotEmpty();
            RuleFor(album => album.Artist).NotNull().NotEmpty();
        }
    }
}