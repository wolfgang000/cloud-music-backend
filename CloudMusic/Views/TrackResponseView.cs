using CloudMusic.Adapters;
using CloudMusic.Models;

namespace CloudMusic.Views
{
    public class TrackResponseView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string MediaUrl { get; set; }
        public double DurationSeconds { get; set; }
        public short Number { get; set; }
        public AlbumBaseView Album { get; set; }
        public ArtistBaseView Artist { get; set; }

        public static TrackResponseView Build(Track song, IObjectStorage objectStorage)
        {
            return new TrackResponseView
            {
                Id = song.Id,
                Title = song.Title,
                MediaUrl = objectStorage.GetURL(song.ObjectStorageKey),
                DurationSeconds = song.DurationSeconds,
                Number = song.Number,
                Album = AlbumBaseView.Build(song.Album, objectStorage),
                Artist = ArtistBaseView.Build(song.Artist, objectStorage)
            };
        }


        public override string ToString()
        {
            return $@"[TrackResponseView (
                Id: {Id}, 
                Title: {Title},
            )]";
        }
    }
}