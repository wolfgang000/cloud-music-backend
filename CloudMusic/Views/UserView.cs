using CloudMusic.Models;

namespace CloudMusic.Views
{
    public class UserView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public static UserView Build(User user)
        {
            return new UserView
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
            };
        }

        public override string ToString()
        {
            return $@"[UserView (
                Id: {Id}, 
                Email: {Email},
            )]";
        }
    }
}