using System.Collections.Generic;
using CloudMusic.Adapters;
using CloudMusic.Models;
using System.Linq;
using System;

namespace CloudMusic.Views
{
    public class ArtistBaseView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public static ArtistBaseView Build(AlbumArtist artist, IObjectStorage objectStorage)
        {
            return new ArtistBaseView
            {
                Id = artist.ArtistId,
                Name = artist.ArtistName
            };
        }

        public static ArtistBaseView Build(Artist artist, IObjectStorage objectStorage)
        {
            return new ArtistBaseView
            {
                Id = artist.Id,
                Name = artist.Name
            };
        }

        public override string ToString()
        {
            return $@"[ArtistBaseView (
                Id: {Id}, 
                Name: {Name},
            )]";
        }
    }
}