using System.Collections.Generic;
using CloudMusic.Adapters;
using CloudMusic.Models;
using System.Linq;
using System;

namespace CloudMusic.Views
{
    public class GenreBaseView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public static GenreBaseView Build(Genre genre)
        {
            return new GenreBaseView
            {
                Id = genre.Id,
                Name = genre.Name
            };
        }


        public override string ToString()
        {
            return $@"[GenreBaseView (
                Id: {Id}, 
                Name: {Name},
            )]";
        }
    }
}