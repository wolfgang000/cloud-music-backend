using System.Collections.Generic;
using CloudMusic.Adapters;
using CloudMusic.Models;
using System.Linq;
using System;

namespace CloudMusic.Views
{
    public class ArtistWithAlbumsView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<AlbumBaseView> Albums { get; set; }

        public static ArtistWithAlbumsView Build(Artist artist, IObjectStorage objectStorage)
        {
            return new ArtistWithAlbumsView
            {
                Id = artist.Id,
                Name = artist.Name,
                Albums = Utils.General.IsNullOrEmpty<Album>(artist.Albums) ?
                new List<AlbumBaseView>() : artist.Albums.Select(album => AlbumBaseView.Build(album, objectStorage)).ToList()
            };
        }

        public override string ToString()
        {
            return $@"[ArtistWithAlbumsView (
                Id: {Id}, 
                Name: {Name},
            )]";
        }
    }
}