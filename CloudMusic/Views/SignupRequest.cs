namespace CloudMusic.Views
{
    public class SignupRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string BetaCode { get; set; }

        public override string ToString()
        {
            return $@"[SignupRequest (
                Email: {Email}, 
            )]";
        }
    }
}