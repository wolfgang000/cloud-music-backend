using CloudMusic.Models;

namespace CloudMusic.Views
{
    public class UserDetailView
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public long StorageLimitSize { get; set; }
        public long UsedStorage { get; set; }

        public static UserDetailView Build(UserDetail user)
        {
            return new UserDetailView
            {
                Name = user.Name,
                Email = user.Email,
                StorageLimitSize = user.StorageLimitSize,
                UsedStorage = user.UsedStorage
            };
        }

        public override string ToString()
        {
            return $@"[UserView (
                Email: {Email},
            )]";
        }
    }
}