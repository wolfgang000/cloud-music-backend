using System.Collections.Generic;
using CloudMusic.Adapters;
using CloudMusic.Models;
using System.Linq;
using System;

namespace CloudMusic.Views
{
    public class AlbumBaseView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string MediaUrl { get; set; }
        public ArtistBaseView Artist { get; set; }
        public List<ArtistBaseView> Artists { get; set; }
        public static AlbumBaseView Build(Album album, IObjectStorage objectStorage)
        {
            return new AlbumBaseView
            {
                Id = album.Id,
                Title = album.Title,
                Artist = ArtistBaseView.Build(album.Artist, objectStorage),
                Artists = Utils.General.IsNullOrEmpty<AlbumArtist>(album.Artists) ? new List<ArtistBaseView>() : album.Artists.Select(a => ArtistBaseView.Build(a, objectStorage)).ToList(),
                MediaUrl = string.IsNullOrWhiteSpace(album.CoverArtObjectStorageKey) ? null : objectStorage.GetURL(album.CoverArtObjectStorageKey)
            };
        }

        public override string ToString()
        {
            return $@"[AlbumBaseView (
                Id: {Id}, 
                Title: {Title},
            )]";
        }
    }
}