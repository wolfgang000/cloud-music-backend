using System.Collections.Generic;
using CloudMusic.Adapters;
using CloudMusic.Models;
using System.Linq;
using System;

namespace CloudMusic.Views
{
    public class AlbumDetailView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string MediaUrl { get; set; }
        public List<TrackResponseView> Tracks { get; set; }
        public ArtistBaseView Artist { get; set; }
        public List<ArtistBaseView> Artists { get; set; }

        public static AlbumDetailView Build(Album album, IObjectStorage objectStorage)
        {
            return new AlbumDetailView
            {
                Id = album.Id,
                Title = album.Title,
                MediaUrl = string.IsNullOrWhiteSpace(album.CoverArtObjectStorageKey) ? null : objectStorage.GetURL(album.CoverArtObjectStorageKey),
                Artist = ArtistBaseView.Build(album.Artist, objectStorage),
                Artists = Utils.General.IsNullOrEmpty<AlbumArtist>(album.Artists) ? new List<ArtistBaseView>() : album.Artists.Select(a => ArtistBaseView.Build(a, objectStorage)).ToList(),
                Tracks = Utils.General.IsNullOrEmpty<Track>(album.Tracks) ?
                new List<TrackResponseView>() : album.Tracks.Select(track => TrackResponseView.Build(track, objectStorage)).ToList()
            };
        }

        public override string ToString()
        {
            return $@"[AlbumDetailView (
                Id: {Id}, 
                Title: {Title},
            )]";
        }
    }
}