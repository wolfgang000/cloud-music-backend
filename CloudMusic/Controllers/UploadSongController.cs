﻿using System;
using System.Collections.Generic;
using HeyRed.Mime;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using CloudMusic.Services;
using CloudMusic.Models;
using CloudMusic.Adapters;
using CloudMusic.Views;
using CloudMusic.Utils;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Authorization;
using CloudMusic.Errors;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/upload_song")]
    public class UploadSongController : ControllerBase
    {
        private readonly IRepositoryService _repo;
        private readonly IGeneralConfig _generalConfig;
        private readonly IObjectStorage _objectStorage;
        private readonly ILogger<UploadSongController> _logger;
        public UploadSongController(
            IRepositoryService repo,
            IGeneralConfig generalConfig,
            IObjectStorage objectStorage,
            ILogger<UploadSongController> logger
        )
        {
            _repo = repo;
            _logger = logger;
            _generalConfig = generalConfig;
            _objectStorage = objectStorage;
        }

        // TODO: add finaly clean up
        // TODO: Add transaction
        [HttpPost]
        [RequestSizeLimit(100000000)]
        public async Task<IActionResult> UploadSong(IFormFile file)
        {
            var userId = General.GetUserId(HttpContext);

            return await Result.Success<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>(new Dictionary<string, dynamic>())
            .Bind(async (accMap) =>
            {
                var userResult = await _repo.UserService.FindById(userId);
                if (userResult.IsFailure)
                {
                    _logger.LogError($"User not found with Id: ${userId}, this should not have happened");
                    return Result.Failure<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>((accMap, new StatusCodeResult(StatusCodes.Status500InternalServerError)));
                }
                accMap.Add("user", userResult.Value);
                accMap.Add("userId", userId);
                accMap.Add("file", file);
                return Result.Success<Dictionary<string, dynamic>, (Dictionary<string, dynamic> AccMap, IActionResult ErrorResponse)>(accMap);
            })
            .Bind(async (accMap) => await TrackUploaderUtil.ValidateUserAvailableStorage(accMap, this._repo))
            .Bind(TrackUploaderUtil.ValidateFileExtension)
            .Bind(async (accMap) => await TrackUploaderUtil.SaveTempFile(accMap, this._generalConfig.TempFilesPath()))
            .Bind(async (accMap) => await TrackUploaderUtil.ValidateIfTrackAlreadyExists(accMap, this._repo))
            .Bind(async (accMap) => await TrackUploaderUtil.ExtractTrackMetaData(accMap, this._repo, this._generalConfig.TempFilesPath()))
            .Bind(async (accMap) => await TrackUploaderUtil.UploadFile(accMap, this._objectStorage))
            .Bind(async (accMap) => await TrackUploaderUtil.SaveEntities(accMap, this._repo, this._logger))
            .Map(accMap =>
            {
                TrackUploaderUtil.CleanUpTempFiles(accMap);
                Track track = accMap["track"];
                track.Album = accMap["album"];
                track.Artist = accMap["trackArtist"];

                return Created(
                    nameof(UploadSongController),
                    TrackResponseView.Build(accMap["track"],
                    _objectStorage)
                );
            })
            .Finally(
                async (result) =>
                {
                    if (result.IsFailure)
                    {
                        var accMap = result.Error.AccMap;
                        TrackUploaderUtil.CleanUpTempFiles(accMap);
                        await TrackUploaderUtil.DeleteObjectStorageFiles(accMap, this._objectStorage);
                        return result.Error.ErrorResponse;
                    }
                    else
                    {
                        return result.Value;
                    }
                }
            );
        }
    }
}
