using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using CloudMusic.Services;
using CloudMusic.Models;
using CloudMusic.Adapters;
using CloudMusic.Views;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using CloudMusic.Errors;
using CloudMusic.Utils;
using Microsoft.AspNetCore.Authorization;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/auth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IUserService _userService;

        public AuthenticationController(
            ILogger<AuthenticationController> logger,
            IUserService userService
        )
        {
            _logger = logger;
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginRequest login)
        {
            var loginResult = await _userService.Authenticate(login.Email, login.Password);

            if (loginResult.IsFailure)
                return BadRequest(loginResult.Error);

            var user = loginResult.Value;


            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true,
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            return Ok(UserView.Build(user));
        }

        [HttpPost("logout")]
        public async Task<ActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public async Task<IActionResult> CreateAccount([FromBody] SignupRequest signupRequest)
        {
            return await Result.Success<User, IActionResult>(
                new User
                {
                    Name = signupRequest.Name,
                    Email = signupRequest.Email,
                    StorageLimitSize = 100000000000
                }
            )
            .Bind((user) =>
            {
                // Validate user's fields 
                var validationResult = UserValidator.Default.Validate(user);
                if (!validationResult.IsValid)
                {
                    Dictionary<string, string[]> errorMap = Utils.General.FormatFluentValidationErrorsIntoDictionary(validationResult.Errors);
                    var errorResponse = new ErrorResponse
                    {
                        Code = -1,
                        Message = "Params error",
                        Errors = errorMap,
                    };
                    return Result.Failure<User, IActionResult>(BadRequest(errorResponse));
                }
                else
                {
                    return Result.Success<User, IActionResult>(user);
                }
            })
            .Bind(async (user) =>
            {
                BetaCode betaCode = null;
                if (signupRequest.BetaCode != "dummycodedonotuse")
                {
                    var betaCodeResult = await _userService.GetBetaCode(signupRequest.BetaCode);
                    if (betaCodeResult.IsFailure)
                    {
                        var errorMap = new Dictionary<string, string[]>();
                        string[] newErrors = { "Invalid code" };
                        errorMap.Add("BetaCode", newErrors);

                        var errorResponse = new ErrorResponse
                        {
                            Code = -1,
                            Message = "Params error",
                            Errors = errorMap,
                        };
                        return Result.Failure<User, IActionResult>(BadRequest(errorResponse));
                    }
                    betaCode = betaCodeResult.Value;
                }
                var userResult = await _userService.Create(user, signupRequest.Password);
                if (userResult.IsFailure)
                {
                    var errorResponse = new ErrorResponse
                    {
                        Code = -1,
                        Message = "Params error",
                        Errors = userResult.Error,
                    };
                    return Result.Failure<User, IActionResult>(BadRequest(errorResponse));
                }
                else
                {
                    if (signupRequest.BetaCode != "dummycodedonotuse")
                        await _userService.DeleteBetaCode(betaCode);

                    return Result.Success<User, IActionResult>(userResult.Value);
                }
            }).Map(user =>
            {
                return Created(
                    nameof(AuthenticationController),
                    UserView.Build(user)
                );
            }).Finally(
                result => result.IsSuccess ? result.Value : result.Error
            );
        }

        [HttpGet("user_detail")]
        public async Task<ActionResult> GetUserDetail()
        {
            var userId = General.GetUserId(HttpContext);
            var userResult = await _userService.GetUserDetailById(userId);
            if (userResult.IsFailure)
            {
                _logger.LogError($"User not found with Id: ${userId}, this should not have happened");
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            var user = userResult.Value;
            var userResponse = UserDetailView.Build(user);
            return Ok(userResponse);
        }
    }
}
