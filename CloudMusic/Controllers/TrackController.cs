
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CloudMusic.Services;
using CloudMusic.Models;
using CloudMusic.Adapters;
using CloudMusic.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using CloudMusic.Utils;
using System.Collections.Generic;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/tracks")]
    public class TrackController : ControllerBase
    {
        private readonly IRepositoryService _repo;
        private readonly IObjectStorage _objectStorage;
        private readonly ILogger<TrackController> _logger;
        public TrackController(IRepositoryService repo, ILogger<TrackController> logger, IObjectStorage objectStorage)
        {
            _logger = logger;
            _repo = repo;
            _objectStorage = objectStorage;
        }

        [HttpGet]
        public async Task<ActionResult> GetTracks()
        {
            var userId = General.GetUserId(HttpContext);

            IEnumerable<Track> tracks = null;
            if (HttpContext.Request.Query.ContainsKey("genreId"))
            {
                long genreId = long.Parse(HttpContext.Request.Query["genreId"].First());
                tracks = await _repo.TrackService.ListByUserIdAndGenreId(userId, genreId);
            }
            else if (HttpContext.Request.Query.ContainsKey("artistId"))
            {
                long artistId = long.Parse(HttpContext.Request.Query["artistId"].First());
                tracks = await _repo.TrackService.ListByUserIdAndArtistId(userId, artistId);
            }
            else
            {
                tracks = await _repo.TrackService.ListByUserId(userId);
            }

            var tracksResponse = tracks.Select(song => TrackResponseView.Build(song, _objectStorage)).ToList();
            return Ok(tracksResponse);
        }
    }
}
