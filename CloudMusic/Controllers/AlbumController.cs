using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using CloudMusic.Services;
using CloudMusic.Models;
using CloudMusic.Adapters;
using CloudMusic.Views;
using Microsoft.AspNetCore.Authorization;
using CloudMusic.Utils;
using System.Collections.Generic;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/albums")]
    public class AlbumController : ControllerBase
    {
        private readonly IAlbumService _albumService;
        private readonly IObjectStorage _objectStorage;
        private readonly ILogger<UploadSongController> _logger;
        public AlbumController(IAlbumService albumService, ILogger<UploadSongController> logger, IObjectStorage objectStorage)
        {
            _logger = logger;
            _albumService = albumService;
            _objectStorage = objectStorage;
        }

        [HttpGet]
        public async Task<ActionResult> GetAlbums()
        {
            var userId = General.GetUserId(HttpContext);
            IEnumerable<Album> albums = null;
            string orderByParams = HttpContext.Request.Query["orderBy"].DefaultIfEmpty("").FirstOrDefault();
            if (HttpContext.Request.Query.ContainsKey("genreId"))
            {
                long genreId = long.Parse(HttpContext.Request.Query["genreId"].First());
                albums = await _albumService.ListByUserIdAndGenreId(userId, genreId, orderByParams);
            }
            else if (HttpContext.Request.Query.ContainsKey("artistId"))
            {
                long artistId = long.Parse(HttpContext.Request.Query["artistId"].First());
                albums = await _albumService.ListByUserIdAndArtistId(userId, artistId);
            }
            else
            {
                albums = await _albumService.ListByUserId(userId, orderByParams);
            }
            var albumsResponse = albums.Select(album => AlbumBaseView.Build(album, _objectStorage)).ToList();
            return Ok(albumsResponse);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAlbumDetail(long id)
        {
            var userId = General.GetUserId(HttpContext);
            var albumResult = await _albumService.DetailFindById(userId, id);

            if (albumResult.IsFailure)
            {
                return NotFound();
            }

            var albumsResponse = AlbumDetailView.Build(albumResult.Value, _objectStorage);
            return Ok(albumsResponse);
        }
    }
}
