
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CloudMusic.Services;
using CloudMusic.Adapters;
using Microsoft.AspNetCore.Authorization;
using CloudMusic.Utils;
using CloudMusic.Views;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/genres")]
    public class GenreController : ControllerBase
    {
        private readonly IRepositoryService _repo;
        private readonly ILogger<GenreController> _logger;
        public GenreController(IRepositoryService repo, IObjectStorage objectStorage, ILogger<GenreController> logger)
        {
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> GetGenres()
        {
            var userId = General.GetUserId(HttpContext);
            var genres = await _repo.GenreService.ListByUserIdWithAlbumsAndTracksCounts(userId);
            return Ok(genres);
        }


        [HttpGet("{id}")]
        public async Task<ActionResult> GetGenre(long id)
        {
            var genreResult = await _repo.GenreService.FindById(id);
            if (genreResult.IsFailure)
            {
                return NotFound();
            }

            var artist = GenreBaseView.Build(genreResult.Value);
            return Ok(artist);
        }
    }
}
