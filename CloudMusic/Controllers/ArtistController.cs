
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CloudMusic.Services;
using CloudMusic.Models;
using CloudMusic.Adapters;
using CloudMusic.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using CloudMusic.Utils;

namespace CloudMusic.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/artists")]
    public class ArtistController : ControllerBase
    {
        private readonly IRepositoryService _repo;
        private readonly IObjectStorage _objectStorage;
        private readonly ILogger<ArtistController> _logger;
        public ArtistController(IRepositoryService repo, IObjectStorage objectStorage, ILogger<ArtistController> logger)
        {
            _repo = repo;
            _objectStorage = objectStorage;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> GetArtists()
        {
            var userId = General.GetUserId(HttpContext);
            var artists = await _repo.ArtistService.ListByUserId(userId);
            var artistsResponse = artists.Select(a => ArtistBaseView.Build(a, _objectStorage)).ToList();
            return Ok(artistsResponse);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetArtist(long id)
        {
            var artistResult = await _repo.ArtistService.FindById(id);
            if (artistResult.IsFailure)
            {
                return NotFound();
            }

            var artist = ArtistBaseView.Build(artistResult.Value, _objectStorage);
            return Ok(artist);
        }
    }
}
