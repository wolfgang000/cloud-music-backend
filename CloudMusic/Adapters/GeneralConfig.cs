namespace CloudMusic.Adapters
{
    public class GeneralConfig : IGeneralConfig
    {
        readonly string _tempFilesPath;

        public GeneralConfig(string tempFilesPath)
        {
            _tempFilesPath = tempFilesPath;
        }

        public string TempFilesPath()
        {
            return _tempFilesPath;
        }
    }
    public interface IGeneralConfig
    {
        string TempFilesPath();
    }
}