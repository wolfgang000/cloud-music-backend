using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CloudMusic.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ThirdParty.Json.LitJson;

namespace CloudMusic.Adapters
{
    public class FileSystemStorage : IObjectStorage
    {
        readonly string _mediaPath;
        readonly string _mediaUrl;
        public FileSystemStorage(string mediaPath, string mediaUrl)
        {
            _mediaPath = mediaPath;
            _mediaUrl = mediaUrl;
        }

        public string GetURL(string key)
        {
            return Path.Join(_mediaUrl, key);
        }

        public async Task UploadAsync(string filePath, string key, string contentType)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(Path.Join(_mediaPath, key)));
            using (
                FileStream inputStream = new FileStream(filePath, FileMode.Open),
                outputStream = new FileStream(Path.Join(_mediaPath, key), FileMode.Create)
            )
            {
                await inputStream.CopyToAsync(outputStream);
            }
        }

        public async Task DeleteAsync(string key)
        {
            await Task.Delay(1);
        }
    }

    // TODO: refactor this using new s3 api
    public class B2Storage : IObjectStorage
    {
        readonly string _apiUrl;
        readonly string _mediaUrl;
        readonly string _authorizationToken;
        readonly string _bucketId;
        readonly IMemoryCache _memoryCache;
        readonly HttpClient _httpClient;
        private readonly ILogger<B2Storage> _logger;


        public B2Storage(IConfiguration configuration, IMemoryCache memoryCache, ILogger<B2Storage> logger)
        {
            _httpClient = new HttpClient();
            _logger = logger;
            _logger.LogInformation("Setting up B2 client");
            var keyID = General.GetConfigurationValue(configuration, "B2_KEY_ID");
            var applicationKey = General.GetConfigurationValue(configuration, "B2_APPLICATION_KEY");
            var bucketId = General.GetConfigurationValue(configuration, "B2_BUCKET_ID");
            _mediaUrl = General.GetConfigurationValue(configuration, "B2_MEDIA_URL");
            _memoryCache = memoryCache;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://api.backblazeb2.com/b2api/v2/b2_authorize_account");
            string credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(keyID + ":" + applicationKey));
            webRequest.Headers.Add("Authorization", "Basic " + credentials);
            webRequest.ContentType = "application/json; charset=utf-8";
            WebResponse response = (HttpWebResponse)webRequest.GetResponse();
            String json = new StreamReader(response.GetResponseStream()).ReadToEnd();
            response.Close();
            JsonData data = JsonMapper.ToObject(json);

            _apiUrl = (string)data["apiUrl"];
            _authorizationToken = (string)data["authorizationToken"];
            _bucketId = bucketId;
            _logger.LogInformation("B2 client was successfully started");
        }

        public string GetURL(string key)
        {
            return Path.Join(_mediaUrl, key);
        }

        private async Task<UploadUrlResponse> GetUploadUrl()
        {
            int attemptsSoFar = 0;
            HttpResponseMessage response = null;
            while (attemptsSoFar < 5)
            {
                try
                {
                    using var requestMessage = new HttpRequestMessage(HttpMethod.Post, _apiUrl + "/b2api/v2/b2_get_upload_url");
                    requestMessage.Headers.TryAddWithoutValidation("Authorization", _authorizationToken);
                    requestMessage.Content = new StringContent("{\"bucketId\":\"" + _bucketId + "\"}", Encoding.UTF8, "application/json");
                    response = await _httpClient.SendAsync(requestMessage);
                    break;
                }
                catch (System.Net.Http.HttpRequestException e)
                {
                    attemptsSoFar++;
                    Console.WriteLine("Error GetUploadUrl:" + e.ToString());
                    if (attemptsSoFar < 5)
                    {
                        Console.WriteLine("wating 3 seg");
                        await Task.Delay(3000);
                    }
                    else
                    {
                        Console.WriteLine("givin up");
                        throw e;
                    }
                }
            }
            // TODO: VALIDATE STATUS CODE
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var responseData = JsonMapper.ToObject<UploadUrlResponse>(jsonResponse);
            return responseData;
        }

        public async Task UploadAsync(string filePath, string key, string contentType)
        {
            for (int i = 0; i < 5; i++)
            {
                var uploadUrlResponse = await GetUploadUrl();

                String fileName = key;
                String sha1Str = HashUtil.GetSHA1HashFromFile(filePath);

                using var requestMessage = new HttpRequestMessage(HttpMethod.Post, uploadUrlResponse.uploadUrl);
                requestMessage.Headers.TryAddWithoutValidation("Authorization", uploadUrlResponse.authorizationToken);
                requestMessage.Headers.TryAddWithoutValidation("X-Bz-File-Name", fileName);
                requestMessage.Headers.TryAddWithoutValidation("X-Bz-Content-Sha1", sha1Str);

                using Stream inputStream = new FileStream(filePath, FileMode.Open);
                using var httpContent = new StreamContent(inputStream);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                requestMessage.Content = httpContent;
                HttpResponseMessage response = null;
                try
                {
                    response = await _httpClient.SendAsync(requestMessage);
                }
                catch (System.Net.Http.HttpRequestException e)
                {
                    _logger.LogWarning($"B2 error attempt ${i}: ${e.Message}");
                    continue;
                }
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    break;
                }
                if (response.StatusCode == HttpStatusCode.TooManyRequests)
                {
                    _logger.LogWarning("Request Error:" + response.StatusCode);
                    if (response.Headers.TryGetValues("Retry-After", out var headerValues))
                    {
                        var retryTimeSec = headerValues.FirstOrDefault();
                        await Task.Delay(5000);
                    }
                    else
                    {
                        await Task.Delay(5000);
                    }
                }
                else if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                {
                    _logger.LogWarning("Request Error:" + response.StatusCode);
                    await Task.Delay(5000);
                    // if (maxDelay < delaySeconds)
                    // {
                    //     // give up -- delay is too long
                    //     return response
                    // }
                    // sleepSeconds(delaySeconds);
                    // delaySeconds = delaySeconds * 2;
                }
                else
                {
                    _logger.LogWarning("Request Error:" + response.StatusCode);
                    await Task.Delay(5000);
                }
            }
        }
        public async Task DeleteAsync(string key)
        {
            await Task.Delay(1);
        }
#pragma warning disable 0649
        struct UploadUrlResponse
        {
            public string bucketId;
            public string uploadUrl;
            public string authorizationToken;
        }
#pragma warning restore 0649
    }
    public interface IObjectStorage
    {
        Task UploadAsync(string filePath, string key, string contentType);
        Task DeleteAsync(string key);
        string GetURL(string key);
    }
}