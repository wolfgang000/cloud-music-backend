using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;

namespace CloudMusic.Services
{
    public class TrackValidator
    {
        private static readonly Models.TrackValidator _DefaultTrackValidator = new Models.TrackValidator();

        public static Models.TrackValidator Default
        {
            get
            {
                return _DefaultTrackValidator;
            }
        }

    }
    public class TrackService : ITrackService
    {
        private readonly CloudMusicContext _dbContext;

        public TrackService(CloudMusicContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Track>> All()
        {
            return await _dbContext.Tracks.AsNoTracking().ToListAsync();
        }
        public async Task<IEnumerable<Track>> AllWithAlbum()
        {
            return await _dbContext.Tracks
                .Include(t => t.Album)
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<Result<Track>> FindById(long id)
        {
            var quickRun = await _dbContext.Tracks
                .Where(a => a.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return quickRun == null ?
                Result.Failure<Track>($"'Song' with ID: ${id} not found.") : Result.Ok(quickRun);
        }

        public async Task<IEnumerable<Track>> ListByUserIdAndGenreId(long userId, long GenreId)
        {
            return await (from a in _dbContext.Albums
                          join t in _dbContext.Tracks
                          on a.Id equals t.AlbumId
                          where a.UserId == userId && t.GenreId == GenreId
                          select t)
                          .Distinct()
                          .Include(t => t.Artist)
                          .Include(t => t.Album)
                            .ThenInclude(a => a.Artist)
                          .OrderBy(t => t.InsertedAt)
                          .ToListAsync();
        }

        public async Task<IEnumerable<Track>> ListByUserIdAndArtistId(long userId, long artistId)
        {

            var queryByTrackArtistId = from a in _dbContext.Albums
                                       join t in _dbContext.Tracks
                                       on a.Id equals t.AlbumId
                                       where a.UserId == userId && t.ArtistId == artistId
                                       select t;

            var queryByAlbumArtistId = from aa in _dbContext.AlbumArtist
                                       join a in _dbContext.Albums on aa.AlbumId equals a.Id
                                       join t in _dbContext.Tracks on a.Id equals t.AlbumId
                                       where a.UserId == userId && aa.ArtistId == artistId
                                       select t;

            return await queryByTrackArtistId
                          .Union(queryByAlbumArtistId)
                          .Distinct()
                          .Include(t => t.Artist)
                          .Include(t => t.Album)
                            .ThenInclude(a => a.Artist)
                          .OrderBy(t => t.InsertedAt)
                          .ToListAsync();
        }

        public async Task<IEnumerable<Track>> ListByUserId(long userId)
        {
            return await (from a in _dbContext.Albums
                          join t in _dbContext.Tracks
                          on a.Id equals t.AlbumId
                          where a.UserId == userId
                          select t)
                          .Distinct()
                          .Include(t => t.Artist)
                          .Include(t => t.Album)
                            .ThenInclude(a => a.Artist)
                          .OrderBy(t => t.InsertedAt)
                          .ToListAsync();
        }

        public async Task<Result<Track>> FindByHashAndUserId(string hash, long userId)
        {
            var quickRun = await _dbContext.Tracks
                .Where(t => t.SHA1Hash == hash && t.UserId == userId)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return quickRun == null ?
                Result.Failure<Track>($"'Song' with hash: ${hash} not found.") : Result.Ok(quickRun);
        }

        public async Task<Result<Track>> SaveOne(Track quickRun)
        {
            // TODO: refactor
            if (quickRun.Id == 0)
            {
                quickRun.InsertedAt = DateTime.Now;
            }
            quickRun.UpdatedAt = DateTime.Now;


            _dbContext.Tracks.Add(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                // Logger Error 
                return Result.Failure<Track>("Error while 'Track':" + e.ToString());
            }
            catch (Exception e)
            {
                // Logger Error 
                return Result.Failure<Track>("Error while 'Track':" + e.ToString());
            }
            return Result.Ok(quickRun);
        }


        public async Task<Result<Track>> UpdateOne(Track quickRun)
        {
            _dbContext.Tracks.Update(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                // Logger Error 
                return Result.Failure<Track>("Error while 'Song', check logs.");
            }
            catch (Exception)
            {
                // Logger Error 
                return Result.Failure<Track>("Error while 'Song', check logs.");
            }
            return Result.Ok(quickRun);
        }

    }
    public interface ITrackService
    {
        Task<IEnumerable<Track>> All();
        Task<IEnumerable<Track>> AllWithAlbum();
        Task<IEnumerable<Track>> ListByUserIdAndGenreId(long userId, long GenreId);
        Task<IEnumerable<Track>> ListByUserIdAndArtistId(long userId, long artistId);
        Task<IEnumerable<Track>> ListByUserId(long userId);
        Task<Result<Track>> FindById(long id);
        Task<Result<Track>> SaveOne(Track quickRun);
        Task<Result<Track>> UpdateOne(Track quickRun);
        Task<Result<Track>> FindByHashAndUserId(string hash, long userId);
    }
}