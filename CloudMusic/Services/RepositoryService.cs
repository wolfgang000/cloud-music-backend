using CloudMusic.Models;

namespace CloudMusic.Services
{
    public class RepositoryService : IRepositoryService
    {
        private readonly CloudMusicContext _dbContext;
        private readonly ITrackService _trackService;
        private readonly IAlbumService _albumService;
        private readonly IArtistService _artistService;
        private readonly IGenreService _genreService;
        private readonly IUserService _userService;

        public RepositoryService(CloudMusicContext dbContext)
        {
            this._dbContext = dbContext;
            _trackService = new TrackService(this._dbContext);
            _albumService = new AlbumService(this._dbContext);
            _artistService = new ArtistService(this._dbContext);
            _genreService = new GenreService(this._dbContext);
            _userService = new UserService(this._dbContext);
        }

        public CloudMusicContext DbContext => _dbContext;
        public ITrackService TrackService => _trackService;
        public IAlbumService AlbumService => _albumService;
        public IArtistService ArtistService => _artistService;
        public IGenreService GenreService => _genreService;
        public IUserService UserService => _userService;
    }
    public interface IRepositoryService
    {
        CloudMusicContext DbContext { get; }
        ITrackService TrackService { get; }
        IAlbumService AlbumService { get; }
        IArtistService ArtistService { get; }
        IGenreService GenreService { get; }
        IUserService UserService { get; }
    }
}