using System;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using CloudMusic.Models.Queries;
using Dapper;

namespace CloudMusic.Services
{
    public class GenreService : IGenreService
    {
        private readonly CloudMusicContext _dbContext;

        public GenreService(CloudMusicContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Result<Genre>> FindById(long id)
        {
            var genre = await _dbContext.Genres
                .Where(a => a.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return genre == null ?
                Result.Failure<Genre>($"'Genre' with ID: ${id} not found.") : Result.Ok(genre);
        }

        private const string QUERY_LIST_ARTISTS_WITH_ALBUMS_AND_ARTISTS_COUNTS = @"
            select 
                g.""Id"", g.""Name"", count(t.""Id"") as ""TracksCount"", count(distinct t.""AlbumId"") as ""AlbumsCount"" 
            from 
                ""Genres"" as g
            join 
                ""Tracks"" as t on t.""GenreId"" = g.""Id""
            join 
                ""Albums"" as a on a.""Id"" = t.""AlbumId"" 
            where 
                a.""UserId"" = @UserId
            group by g.""Id""
        ";
        public async Task<IEnumerable<GenreWithAlbumsAndArtistsCounts>> ListByUserIdWithAlbumsAndTracksCounts(long userId)
        {
            var conn = _dbContext.Database.GetDbConnection();
            var result = await conn.QueryAsync<GenreWithAlbumsAndArtistsCounts>(QUERY_LIST_ARTISTS_WITH_ALBUMS_AND_ARTISTS_COUNTS, new { UserId = userId });
            return result;
        }

        public async Task<Result<Genre>> GetByNameOrCreate(string name)
        {
            var genre = await _dbContext.Genres
                .Where(a => a.Name == name)
                .FirstOrDefaultAsync();
            if (genre == null)
            {
                return await SaveOne(new Genre { Name = name });
            }
            return Result.Ok(genre);
        }

        public async Task<Result<Genre>> SaveOne(Genre quickRun)
        {
            if (quickRun.Id == 0)
            {
                quickRun.InsertedAt = DateTime.Now;
            }

            _dbContext.Genres.Add(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Result.Failure<Genre>("Error while 'Artist':" + e.ToString());
            }
            return Result.Ok(quickRun);
        }

    }
    public interface IGenreService
    {
        Task<Result<Genre>> FindById(long id);
        Task<Result<Genre>> SaveOne(Genre genre);
        Task<Result<Genre>> GetByNameOrCreate(string name);
        Task<IEnumerable<GenreWithAlbumsAndArtistsCounts>> ListByUserIdWithAlbumsAndTracksCounts(long userId);
    }
}
