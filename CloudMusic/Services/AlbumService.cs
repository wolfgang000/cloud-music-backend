using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;
using CloudMusic.Utils.QueryExtensions;

namespace CloudMusic.Services
{
    public class AlbumValidator
    {
        private static readonly Models.AlbumValidator _DefaultAlbumValidator = new Models.AlbumValidator();

        public static Models.AlbumValidator Default
        {
            get
            {
                return _DefaultAlbumValidator;
            }
        }

    }
    public class AlbumService : IAlbumService
    {
        private readonly CloudMusicContext _dbContext;

        public AlbumService(CloudMusicContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Album>> All()
        {
            return await _dbContext.Albums.ToListAsync();
        }

        public async Task<IEnumerable<Album>> ListByUserId(long userId, string sortByParams = "")
        {
            return await _dbContext.Albums
                .Where(a => a.UserId == userId)
                .SortBy(sortByParams)
                .Include(a => a.Artist)
                .Include(a => a.Artists)
                .ToListAsync();
        }

        public async Task<IEnumerable<Album>> ListByUserIdAndGenreId(long userId, long GenreId, string sortByParams = "")
        {
            return await (from a in _dbContext.Albums
                          join t in _dbContext.Tracks
                          on a.Id equals t.AlbumId
                          where a.UserId == userId && t.GenreId == GenreId
                          select a)
                          .SortBy(sortByParams)
                          .Distinct()
                          .Include(a => a.Artist)
                          .Include(a => a.Artists)
                          .ToListAsync();
        }

        public async Task<IEnumerable<Album>> ListByUserIdAndArtistId(long userId, long artistId)
        {
            return await _dbContext.Albums
                            .Where(a => a.UserId == userId)
                            .Include(a => a.Artist)
                            .Include(a => a.Artists)
                            .Join(_dbContext.AlbumArtist,
                                album => album.Id,
                                albumArtist => albumArtist.AlbumId,
                            (album, albumArtist) => new
                            {
                                Album = album,
                                AlbumArtist = albumArtist
                            })
                            .Where(joinA => joinA.AlbumArtist.ArtistId == artistId)
                            .Select(joinA => joinA.Album)
                            .ToListAsync();
        }

        public async Task<Result<Album>> DetailFindById(long userId, long id)
        {
            var quickRun = _dbContext.Albums
                .Where(a => a.Id == id && a.UserId == userId)
                .Include(a => a.Artist)
                .Include(a => a.Artists)
                .Include(a => a.Tracks)
                    .ThenInclude(t => t.Artist)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            quickRun.Result.Tracks = quickRun.Result.Tracks.OrderBy(t => t.Number).ToList();
            var album = await quickRun;

            return album == null ?
                Result.Failure<Album>($"'Album' with ID: ${id} not found.") : Result.Ok(album);
        }

        public async Task<Result<Album>> FindByTitleAndArtist(string title, string artist)
        {
            var quickRun = await _dbContext.Albums
                    .Include(a => a.Artist)
                    .Where(a => a.Artist.Name == artist && a.Title == title)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

            return quickRun == null ?
                Result.Failure<Album>($"'Album' not found.") : Result.Ok(quickRun);
        }


        public async Task<Result<Album>> SaveOne(Album quickRun)
        {
            // TODO: refactor
            if (quickRun.InsertedAt == null)
            {
                quickRun.InsertedAt = DateTime.Now;
            }

            _dbContext.Albums.Add(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                // Logger Error 
                return Result.Failure<Album>("Error while 'Album':" + e.ToString());
            }
            catch (Exception e)
            {
                // Logger Error 
                return Result.Failure<Album>("Error while 'Album':" + e.ToString());
            }
            return Result.Ok(quickRun);
        }


        public async Task<Result<Album>> UpdateOne(Album quickRun)
        {
            _dbContext.Albums.Update(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                // Logger Error 
                return Result.Failure<Album>("Error while 'Song', check logs.");
            }
            catch (Exception)
            {
                // Logger Error 
                return Result.Failure<Album>("Error while 'Song', check logs.");
            }
            return Result.Ok(quickRun);
        }

    }
    public interface IAlbumService
    {
        Task<IEnumerable<Album>> All();
        Task<IEnumerable<Album>> ListByUserId(long userId, string sortByParams = "");
        Task<IEnumerable<Album>> ListByUserIdAndGenreId(long userId, long GenreId, string sortByParams = "");
        Task<IEnumerable<Album>> ListByUserIdAndArtistId(long userId, long artistId);
        Task<Result<Album>> DetailFindById(long userId, long id);
        Task<Result<Album>> FindByTitleAndArtist(string title, string artist);
        Task<Result<Album>> SaveOne(Album quickRun);
        Task<Result<Album>> UpdateOne(Album quickRun);
    }
}