using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;
using Dapper;
using CloudMusic.Models.Queries;

namespace CloudMusic.Services
{
    public class ArtistService : IArtistService
    {
        private readonly CloudMusicContext _dbContext;

        public ArtistService(CloudMusicContext dbContext)
        {
            _dbContext = dbContext;
        }

        private const string QUERY_GET_USERS_ARTISTS = @"
            select distinct 
                art.* 
            from 
                ""Artists"" art
            join 
                ""Albums"" alb on alb.""ArtistId"" = art.""Id"" 
            where 
                alb.""UserId"" = @UserId
            union select distinct 
                art.* 
            from 
                ""Albums"" alb
            join
                ""Tracks"" tra on tra.""AlbumId"" = alb.""Id""
            join 
                ""Artists"" art on tra.""ArtistId"" = art.""Id"" 
            where 
                alb.""UserId"" = @UserId
        ";
        public async Task<IEnumerable<Artist>> ListByUserId(long userId)
        {
            var conn = _dbContext.Database.GetDbConnection();
            var result = await conn.QueryAsync<Artist>(QUERY_GET_USERS_ARTISTS, new { UserId = userId });
            return result;
        }


        public async Task<Result<Artist>> FindById(long id)
        {
            var artist = await _dbContext.Artists
                .Where(a => a.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return artist == null ?
                Result.Failure<Artist>($"'Artist' with ID: ${id} not found.") : Result.Ok(artist);
        }


        public async Task<Result<Artist>> GetByNameOrCreate(string name)
        {
            var artist = await _dbContext.Artists
                .Where(a => a.Name == name)
                .FirstOrDefaultAsync();
            if (artist == null)
            {
                return await SaveOne(new Artist { Name = name });
            }
            return Result.Ok(artist);
        }

        public async Task<Result<Artist>> SaveOne(Artist quickRun)
        {
            if (quickRun.Id == 0)
            {
                quickRun.InsertedAt = DateTime.Now;
            }

            _dbContext.Artists.Add(quickRun);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Result.Failure<Artist>("Error while 'Artist':" + e.ToString());
            }
            return Result.Ok(quickRun);
        }

        private const string QUERY_LIST_ARTISTS_WITH_ALBUMS_AND_TRACKS_COUNTS = @"
            select 
                a.""Id"", a.""Name"", count(t.""Id"") as ""TracksCount"", count(distinct t.""AlbumId"") as ""AlbumsCount"" 
            from 
                ""Artists"" as a
            join 
                ""AlbumArtistView"" as aav on aav.""ArtistId"" = a.""Id""
            join 
                ""Albums"" as al on al.""Id"" = aav.""AlbumId"" 
            join 
                ""Tracks"" as t on t.""AlbumId"" = al.""Id""
            where 
                al.""UserId"" = @UserId
            group by a.""Id""
        ";
        public async Task<IEnumerable<ArtistWithAlbumsAndArtistsCounts>> ListByUserIdWithAlbumsAndTracksCounts(long userId)
        {
            var conn = _dbContext.Database.GetDbConnection();
            var result = await conn.QueryAsync<ArtistWithAlbumsAndArtistsCounts>(QUERY_LIST_ARTISTS_WITH_ALBUMS_AND_TRACKS_COUNTS, new { UserId = userId });
            return result;
        }

    }
    public interface IArtistService
    {
        Task<IEnumerable<Artist>> ListByUserId(long userId);
        Task<Result<Artist>> FindById(long id);
        Task<Result<Artist>> SaveOne(Artist artist);
        Task<Result<Artist>> GetByNameOrCreate(string name);
        Task<IEnumerable<ArtistWithAlbumsAndArtistsCounts>> ListByUserIdWithAlbumsAndTracksCounts(long userId);
    }
}