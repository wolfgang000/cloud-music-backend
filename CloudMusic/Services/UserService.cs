using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;
using Isopoh.Cryptography.Argon2;
using System.Security.Cryptography;
using CloudMusic.Utils;
using EntityFramework.Exceptions.Common;
using Dapper;
using Npgsql;
using CloudMusic.Adapters;

namespace CloudMusic.Services
{
    public class UserValidator
    {
        private static readonly Models.UserValidatorDefault _DefaultTrackValidator = new Models.UserValidatorDefault();

        public static Models.UserValidatorDefault Default
        {
            get
            {
                return _DefaultTrackValidator;
            }
        }

    }
    public class UsedStorageSizeResultType
    {
        public long UsedStorage { get; set; }
        public long UserId { get; set; }
    };
    public class UserService : IUserService, IDisposable
    {
        private readonly CloudMusicContext _dbContext;
        private readonly RNGCryptoServiceProvider _rngCsp;
        private const short _saltByteLength = 16;

        public UserService(CloudMusicContext dbContext)
        {
            _dbContext = dbContext;
            _rngCsp = new RNGCryptoServiceProvider();
        }

        private byte[] GenerateSalt()
        {
            byte[] salt = new byte[_saltByteLength];
            _rngCsp.GetBytes(salt);
            return salt;
        }

        private void CreatePasswordHash(string password, out string passwordHash, out string passwordSalt)
        {
            byte[] salt = GenerateSalt();
            passwordSalt = HashUtil.ByteArrayToString(salt);
            passwordHash = Argon2.Hash(passwordSalt + password);
        }

        private bool VerifyPasswordHash(string password, string passwordHash, string passwordSalt)
        {
            return Argon2.Verify(passwordHash, passwordSalt + password);
        }

        public async Task<Result<User>> Authenticate(string email, string password)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(x => x.Email == email);
            if (user == null || !VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return Result.Failure<User>($"Bad email or password");

            return Result.Ok(user);
        }

        public async Task<Result<User>> FindById(long id)
        {
            var user = await _dbContext.Users
                .Where(u => u.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return user == null ?
                Result.Failure<User>($"'User' with ID: ${id} not found.") : Result.Ok(user);
        }

        public async Task<Result<UserDetail>> GetUserDetailById(long id)
        {
            var user = await _dbContext.UsersDetail
                .Where(u => u.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return user == null ?
                Result.Failure<UserDetail>($"'User' with ID: ${id} not found.") : Result.Ok(user);
        }

        public async Task<long> GetUsedStorageSize(long userId)
        {
            const string query = @"
                select sum(v_user.t_size) as ""UsedStorage""
                from (
                    select COALESCE(sum(t.""Size""), 0) as t_size
                    from ""Tracks"" as t
                    where t.""UserId"" = @UserId
                    union 
                    select COALESCE(sum(a.""Size""), 0) as t_size
                    from ""Albums"" as a 
                    where a.""UserId"" = @UserId
                ) as v_user
            ";
            var conn = _dbContext.Database.GetDbConnection();
            var result = await conn.QueryAsync<Services.UsedStorageSizeResultType>(query, new { UserId = userId });
            return result.First().UsedStorage;
        }

        public async Task<Result<User, Dictionary<string, string[]>>> Create(User user, string password)
        {
            CreatePasswordHash(password, out var passwordHash, out var passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.Email = user.Email.ToLower();

            user.InsertedAt = DateTime.Now;

            _dbContext.Users.Add(user);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (UniqueConstraintException)
            {
                var errorMap = new Dictionary<string, string[]>();
                string[] newErrors = { "This email is already taken." };
                errorMap.Add("Email", newErrors);

                return Result.Failure<User, Dictionary<string, string[]>>(errorMap);
            }

            return Result.Ok<User, Dictionary<string, string[]>>(user);
        }

        public async Task<Result<User, Dictionary<string, string[]>>> Update(User user)
        {
            _dbContext.Users.Update(user);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (UniqueConstraintException)
            {
                var errorMap = new Dictionary<string, string[]>();
                string[] newErrors = { "This email is already taken." };
                errorMap.Add("Email", newErrors);

                return Result.Failure<User, Dictionary<string, string[]>>(errorMap);
            }
            return Result.Ok<User, Dictionary<string, string[]>>(user);
        }


        public async Task<Result<BetaCode>> GetBetaCode(string code)
        {
            var betaCode = await _dbContext.BetaCodes
                .Where(b => b.Code == code)
                .FirstOrDefaultAsync();

            return betaCode == null ?
                Result.Failure<BetaCode>($"'BetaCode' with code: ${code} not found.") : Result.Ok(betaCode);
        }

        public async Task<Result> DeleteBetaCode(BetaCode betaCode)
        {
            _dbContext.BetaCodes.Remove(betaCode);
            await _dbContext.SaveChangesAsync();
            return Result.Ok();
        }

        public void Dispose()
        {
            _rngCsp.Dispose();
        }
    }
    public interface IUserService
    {
        Task<Result<User>> Authenticate(string email, string password);
        Task<Result<User>> FindById(long id);
        Task<Result<UserDetail>> GetUserDetailById(long id);
        Task<Result<User, Dictionary<string, string[]>>> Create(User user, string password);
        Task<Result<User, Dictionary<string, string[]>>> Update(User user);
        Task<long> GetUsedStorageSize(long userId);
        Task<Result<BetaCode>> GetBetaCode(string code);
        Task<Result> DeleteBetaCode(BetaCode betaCode);
    }
}