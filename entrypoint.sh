#!/bin/sh
# Docker entrypoint script.

# Wait until Postgres is ready
while ! pg_isready -q -h $ASPNETCORE_DB_HOST -p $ASPNETCORE_DB_PORT -U $ASPNETCORE_DB_USER
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

dotnet CloudMusicMigrator.dll

dotnet CloudMusic.dll --urls="http://0.0.0.0:$PORT"