﻿using System;
using CloudMusic.Utils;
using CloudMusic.Models;
using Microsoft.EntityFrameworkCore;

namespace CloudMusicMigrator
{
    class Program
    {
        public static string GetConfigurationValue(string key)
        {
            var value = Environment.GetEnvironmentVariable(key);
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception($"'{key}' property not defined");
            }
            return value;
        }
        static void Main(string[] args)
        {
            string host = GetConfigurationValue("ASPNETCORE_DB_HOST");
            string port = GetConfigurationValue("ASPNETCORE_DB_PORT");
            string user = GetConfigurationValue("ASPNETCORE_DB_USER");
            string password = GetConfigurationValue("ASPNETCORE_DB_PASSWORD");
            string name = GetConfigurationValue("ASPNETCORE_DB_NAME");
            string connectionString = General.GetConnectionString(host, name, user, password);

            DbContextOptions<CloudMusicContext> dbContextOptions = new DbContextOptionsBuilder<CloudMusicContext>()
                .UseNpgsql(connectionString)
                .Options;
            using (var dbContext = new CloudMusicContext(dbContextOptions))
            {
                Console.WriteLine("Starting Database migration");
                dbContext.Database.Migrate();
                Console.WriteLine("The Database was successfully migrated");
            }
        }
    }
}
